//*******************************************************************
//   file: MainWindow.cxx
//
// author: Adam Russell
//
//   mods: J H Stewman (8/2/2005)
//         -- reformatting of code and addition of comment blocks
//         -- corrected mnemonic & accelerator code in main window menus
//
//*******************************************************************

#include <time.h> //***1.85
#include <set>

#include <gdk/gdkkeysyms.h>

#include <pango/pango-font.h>

#include "../support.h"
#include "MainWindow.h"

#include "ModifyDatabaseWindow.h"
#include "AboutDialog.h"
#include "CatalogSchemeDialog.h" //***1.4
#include "ContourInfoDialog.h"
#include "ImageViewDialog.h"
//#include "ErrorDialog.h"
#include "MatchingQueueDialog.h"
#include "OpenFileSelectionDialog.h"
#include "OpenFileChooserDialog.h" //***1.4
#include "SaveFileChooserDialog.h" //***1.99
#include "OptionsDialog.h"
#include "CreateDatabaseDialog.h" //***1.85 
#include "DataExportDialog.h" //***1.9
#include "ExportFinzDialog.h" //***1.99
#include "../CatalogSupport.h" //***1.99

#include "../thumbnail.h" //***1.85

#include "../image_processing/ImageMod.h" //***1.8

#pragma warning (disable : 4305 4309)
#ifndef WIN32
#pragma GCC diagnostic ignored "-Wwrite-strings"
#endif
#include "../../pixmaps/add_database.xpm"
#include "../../pixmaps/about_small.xpm"
#include "../../pixmaps/exit.xpm"
#include "../../pixmaps/exit_small.xpm"
#include "../../pixmaps/icon.xpm"
#include "../../pixmaps/magnify_cursor.xbm"
#include "../../pixmaps/matching_queue.xpm"
#include "../../pixmaps/matching_queue_small.xpm"
#include "../../pixmaps/next.xpm"
#include "../../pixmaps/open_image.xpm"
#include "../../pixmaps/open_image_small.xpm"
#include "../../pixmaps/open_trace.xpm"
#include "../../pixmaps/open_trace_small.xpm"
#include "../../pixmaps/options.xpm"
#include "../../pixmaps/options_small.xpm"
#include "../../pixmaps/previous.xpm"
#include "../../pixmaps/fin.xpm" //***1.5
#include "../../pixmaps/magnify.xpm" //***1.9

#include "../image_processing/transform.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

using namespace std;

static const int IMAGE_WIDTH = 400, IMAGE_HEIGHT = 300; //***1.7 - these were 300 x 250
static const int POINT_SIZE = 1;
static const char *NONE_SUBSTITUTE_STRING = _("(Not Entered)");

//*******************************************************************
MainWindow::MainWindow(Database *db, Options *o)
	: mDBCurEntry(0),
	mSelectedFin(NULL),
	mDatabase(db),
	mIndividualsTblRowCnt(0), // KLY
	mImage(NULL),
	mOrigImage(NULL), //***1.99
	mImageFullsize(NULL), //***2.01
	mOrigImageFullsize(NULL), //***2.01
	// mCList(NULL), //***1.95
	mView(NULL), // KLY
	mStore(NULL), // KLY
	mModel(NULL), // KLY
	mIter(), // KLY
	mSelection(NULL), // KLY
	mCurImageHeight(IMAGE_HEIGHT),
	mCurImageWidth(IMAGE_WIDTH),
	mCurContourHeight(IMAGE_HEIGHT),
	mCurContourWidth(IMAGE_WIDTH),
	mGC(NULL),
	mCursor(NULL),
	mOptions(o),
	mOldSort(DB_SORT_ID), //***1.85
	mNewSort(DB_SORT_ID),  //***1.85
	mImportFromFilename(""), //***1.85
	mExportToFilename(""), //***1.85
	mShowAlternates(false), //***1.95
	mDBCurEntryOffset(0) //***1.96a
{
	// do this here so the database filename can be placed on the window title
	mWindow = createMainWindow(o->mToolbarDisplay); //***1.85

	// create an emergency backup of DB file, as long as it was successfully loaded
	if (mDatabase->status() == Database::loaded)
	{
		string area = mOptions->mCurrentSurveyArea;
		area = area.substr(area.rfind(PATH_SLASH) + 1);
		string backupName = mOptions->mDarwinHome + PATH_SLASH
			+ "system" + PATH_SLASH + "lastLoadOf_" + area + "_"
			+ mOptions->mDatabaseFileName.substr(
			mOptions->mDatabaseFileName.rfind(PATH_SLASH) + 1);
#ifdef WIN32
		string command = "copy /Y /V \"" + mOptions->mDatabaseFileName
#else
		string command = "cp \"" + mOptions->mDatabaseFileName //***2.22 - for Mac
#endif
			+ "\" \"" + backupName + "\" >nul";
		system(command.c_str());
	}
}

//*******************************************************************
MainWindow::~MainWindow()
{
	if (NULL != mWindow)
		gtk_widget_destroy(mWindow);

	delete mImage;
	delete mOrigImage; //***1.99
	delete mImageFullsize; //***2.01
	delete mOrigImageFullsize; //***2.01
	delete mSelectedFin;
	delete mDatabase;
	// delete mIndividualsRecords;

	if (NULL != mGC)
		gdk_gc_unref(mGC);

	if (NULL != mCursor)
		gdk_cursor_destroy(mCursor);

	gtk_main_quit();
}

//*******************************************************************

void MainWindow::setDatabasePtr(Database *db) //***1.85 - used when opening new DB
{
	mDatabase = db; // assume existing database was deleted by caller
}

//*******************************************************************

void MainWindow::setExportFilename(std::string filename) //***1.99
{
	mExportToFilename = filename;
}

//*******************************************************************

int MainWindow::getSelectedRow() //***1.96a - new function
{
	return mDBCurEntry;
}

char * MainWindow::updateMsg(int catID)
{ 
	string catName = "Damage category: " + mDatabase->catCategoryName(catID);
	char * cstr = new char[catName.length() + 1];
	std::strcpy(cstr, catName.c_str());

	return cstr;
}

//*******************************************************************
void MainWindow::show()
{
	//***1.85 - this function is now called ONLY after a database is intially loaded
	// so we reset entries, list sort order, etc. here

	mOldSort = DB_SORT_ID;
	mNewSort = DB_SORT_ID;

	/* Disable clearText. Removing references to mSearchID - Mod by KLY
	clearText(); */
	refreshOptions(true, false); // ***1.96a - this is the initial showing
	refreshDisplayFromDB();

	// do not show (first time) until list is built from database file
	gtk_widget_show(mWindow);
	
	updateGC();
	updateCursor();
	//refreshOptions(); 
	refreshImage();                 //***1.85
	refreshOutline();               //***1.85

	if (mIndividualsTblRowCnt > 0){				//***002DB
		gtk_widget_set_sensitive(mButtonModify, TRUE);  //***002DB
	}							//***002DB
	else {
		gtk_widget_set_sensitive(mButtonNext, FALSE);
		gtk_widget_set_sensitive(mButtonPrev, FALSE);
		gtk_widget_set_sensitive(mButtonModify, FALSE); //***002DB
	}

	char numEntriesStr[64];
	sprintf(numEntriesStr, "Ready: %d fins loaded", mIndividualsTblRowCnt);
	this->displayStatusMessage(numEntriesStr); //***2.01 - display size too
	
}

//*******************************************************************
//***1.85 - new function to set up after loading or importing DB
//
void MainWindow::resetTitleButtonsAndBackupOnDBLoad()
{
	// reset the database filename for the window
	string MainWinTitle = "DARWIN - ";
	switch (mDatabase->status())
	{
	case Database::loaded:
		MainWinTitle += mOptions->mDatabaseFileName;
		break;
	case Database::fileNotFound:
		MainWinTitle += "Database file not found";
		break;
	case Database::errorCreating:
		MainWinTitle += "Database file could not be created";
		break;
	case Database::errorLoading:
		MainWinTitle += "Error loading database file";
		break;
	case Database::oldDBVersion:
		MainWinTitle += "OLD database file version - not supported";
		break;
	default:
		MainWinTitle += "unknown error with database file";
		break;
	}
	gtk_window_set_title(GTK_WINDOW(mWindow), _(MainWinTitle.c_str()));

	// set sensitivity of menu items and buttons depending on success
	if (mDatabase->status() == Database::loaded)
	{
		gtk_widget_set_sensitive(mOpenImageMenuItem, TRUE);
		gtk_widget_set_sensitive(mOpenFinMenuItem, TRUE);
		gtk_widget_set_sensitive(mQueueMenuItem, TRUE);
		gtk_widget_set_sensitive(mBackupMenuItem, TRUE);
		gtk_widget_set_sensitive(mExportDBMenuItem, TRUE);
		gtk_widget_set_sensitive(mOpenImageButton, TRUE);
		gtk_widget_set_sensitive(mOpenFinButton, TRUE);
		gtk_widget_set_sensitive(mQueueButton, TRUE);
		gtk_widget_set_sensitive(mExportSubMenuItem, TRUE);
		gtk_widget_set_sensitive(mImportFinzMenuItem, TRUE);
	}
	else
	{
		gtk_widget_set_sensitive(mOpenImageMenuItem, FALSE);

		gtk_widget_set_sensitive(mOpenFinMenuItem, FALSE);
		gtk_widget_set_sensitive(mQueueMenuItem, FALSE);
		gtk_widget_set_sensitive(mBackupMenuItem, FALSE);
		gtk_widget_set_sensitive(mExportDBMenuItem, FALSE);
		gtk_widget_set_sensitive(mOpenImageButton, FALSE);
		gtk_widget_set_sensitive(mOpenFinButton, FALSE);
		gtk_widget_set_sensitive(mQueueButton, FALSE);
		gtk_widget_set_sensitive(mExportSubMenuItem, FALSE);
		gtk_widget_set_sensitive(mImportFinzMenuItem, FALSE);
	}

	// create an emergency backup of DB file, as long as it was successfully loaded
	if (mDatabase->status() == Database::loaded)
	{
		string area = mOptions->mCurrentSurveyArea;
		area = area.substr(area.rfind(PATH_SLASH) + 1);
		string backupName = mOptions->mDarwinHome + PATH_SLASH
			+ "system" + PATH_SLASH + "lastLoadOf_" + area + "_"
			+ mOptions->mDatabaseFileName.substr(
			mOptions->mDatabaseFileName.rfind(PATH_SLASH) + 1);
#ifdef WIN32
		string command = "copy /Y /V \"" + mOptions->mDatabaseFileName
#else
		string command = "cp \"" + mOptions->mDatabaseFileName //***2.22 - for Mac
#endif
			+ "\" \"" + backupName + "\" >nul";
		system(command.c_str());
	}
	mDBCurEntryOffset = 0; //We have a new database, clear the offset to force image rebuilt, etc.//SAH-DB
}

//*******************************************************************
void MainWindow::refreshImage()
{
	if (/*TRUE == */GDK_IS_DRAWABLE(mDrawingAreaImage->window)) // notebook page is visible
		on_mainDrawingAreaImage_expose_event(mDrawingAreaImage, NULL, (void *) this);
}

//*******************************************************************
void MainWindow::refreshOrigImage()
{
	if (/*TRUE == */GDK_IS_DRAWABLE(mDrawingAreaOrigImage->window)) // notebook page is visible
		on_mainDrawingAreaOrigImage_expose_event(mDrawingAreaOrigImage, NULL, (void *) this);
}

//*******************************************************************
void MainWindow::refreshOutline()
{
	if (/*TRUE == */GDK_IS_DRAWABLE(mDrawingAreaOutline->window)) // notebook page is visible
		on_mainDrawingAreaOutline_expose_event(mDrawingAreaOutline, NULL, (void *) this);
}

//*******************************************************************
void MainWindow::clearText()				//***002DB - nf
{
	/* No longer needed as we are using GtkTree built in seach feature.  Mod by KLY. */
	gtk_entry_set_text(GTK_ENTRY(mSearchID), ""); //***1.85 - always clear this

	if (mDatabase->size() != 0)
		return;

	gtk_entry_set_text(GTK_ENTRY(mEntryID), " ");
	gtk_entry_set_text(GTK_ENTRY(mEntryName), " ");
	gtk_entry_set_text(GTK_ENTRY(mEntryDate), " ");
	gtk_entry_set_text(GTK_ENTRY(mEntryRoll), " ");
	gtk_entry_set_text(GTK_ENTRY(mEntryLocation), " ");
	gtk_entry_set_text(GTK_ENTRY(mEntryDescription), " ");

	gtk_widget_set_sensitive(mEntryID, FALSE);
	gtk_widget_set_sensitive(mEntryName, FALSE);
	gtk_widget_set_sensitive(mEntryDate, FALSE);
	gtk_widget_set_sensitive(mEntryRoll, FALSE);
	gtk_widget_set_sensitive(mEntryLocation, FALSE);
	gtk_widget_set_sensitive(mEntryDescription, FALSE);

	gtk_frame_set_label(GTK_FRAME(mFrameMod), "ID Code");
	gtk_frame_set_label(GTK_FRAME(mFrameOrig), "ID Code");

	return;

}

//*******************************************************************
// Reloading the entire database file
//          
//
void MainWindow::refreshDisplayFromDB()
{
	try {
		std::list<DBIndividual>* individuals = new std::list<DBIndividual>();
		std::list<DBIndividual> individualsDataset = std::list<DBIndividual>();
		DBIndividual individual;
		time_t starttime,endtime;
		double seconds;

		time(&starttime);  
		/* get current time; same as: now = time(NULL)  */
		/* Get the rows from the Individuals table. */
		individualsDataset = mDatabase->DatabaseRows();
		/* Point to our dataset. */
		individuals = &individualsDataset;
		/* How many rows? */
		mIndividualsTblRowCnt = individuals->size();

		if (mIndividualsTblRowCnt == 0){            //***002DB >
			gtk_widget_set_sensitive(mButtonNext, FALSE);
			gtk_widget_set_sensitive(mButtonPrev, FALSE);
			gtk_widget_set_sensitive(mButtonModify, FALSE);

			// we may be loading an empty database so make sure leftovers
			// from previous database are freed
			if (NULL != mSelectedFin)
			{
				delete mSelectedFin;
				mSelectedFin = NULL;
			}
			if (NULL != mImage)
			{
				delete mImage;
				mImage = NULL;
			}
		}					//***002DB <
		else 
			gtk_widget_set_sensitive(mButtonModify, TRUE); //**003MR

		//***1.85 - set font as currently selected 
		gtk_widget_modify_font(
			mView,
			(pango_font_description_from_string(mOptions->mCurrentFontName.c_str())));


		// Some variables for the pixmap display
		// GdkPixmap *pixmap = NULL;
		GdkPixbuf *pixbuf = NULL;

		// Some variables for our TreeView.
		GtkTreeIter iter;
		gint sort_column_id;
		GtkSortType sort_order;
		GtkTreePath *path;

		mSelection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mView));

		/* Speed Issues when Adding a Lot of Rows
		A common scenario is that a model needs to be filled with a lot of rows at some point,
		either at start-up, or when some file is opened. An equally common scenario is that this
		takes an awfully long time even on powerful machines once the model contains more than
		a couple of thousand rows, with an exponentially decreasing rate of insertion.
		Writing a custom model might be the best thing to do in this case.
		Nevertheless, there are some things you can do to work around this problem and speed things
		up a bit even with the stock Gtk+ models:

		Firstly, you should detach your list store or tree store from the tree view before doing
		your mass insertions, then do your insertions, and only connect your store to the tree view
		again when you are done with your insertions.

		Secondly, you should make sure that sorting is disabled while you are doing your mass insertions,
		otherwise your store might be resorted after each and every single row insertion, which is going
		to be everything but fast.

		Thirdly, you should not keep around a lot of tree row references if you have so many rows,
		because with each insertion (or removal) every single tree row reference will check whether its
		path needs to be updated or not.
		*/
		/* Get the model. */
		mModel = gtk_tree_view_get_model(GTK_TREE_VIEW(mView));
		/* Get the store. */
		mStore = GTK_TREE_STORE(mModel);
		/* Remove all rows from the store. */
		gtk_tree_store_clear(mStore);
		// Store current sorting information and store in &sort_column_id, &sort_order.
		gtk_tree_sortable_get_sort_column_id(GTK_TREE_SORTABLE(mModel), &sort_column_id, &sort_order);
		// Set sort order to unsorted.
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(mModel), GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID,
			GTK_SORT_ASCENDING);

		/* No more than one item can be selected */
		gtk_tree_selection_set_mode(mSelection, GTK_SELECTION_SINGLE);
		/* Make sure the model stays with us after the tree view unrefs it */
		g_object_ref(mStore); 
		/* Detach model from view */
		gtk_tree_view_set_model(GTK_TREE_VIEW(mView), NULL); 
	
		/* Now start adding your rows to the store. */
		cout << "Creating Main Window List From Database" << endl;

		int i = 0;
		while (!individuals->empty()) {
			individual = individuals->front();
			individuals->pop_front();

			if (0 == i % 10)
				cout << ".";

			DatabaseFin<ColorImage> *fin = mDatabase->getItemAbsolute(individual.id);

			/*********************************************************/
			/* *** FYI: fin->mDataPos is actually individuals.id *** */
			/*********************************************************/

			/* gdk_pixmap_colormap_create_from_xpm_d has been deprecated since version 2.22 and should not be used in newly-written code. Use a GdkPixbuf instead. */
			pixbuf = gdk_pixbuf_new_from_xpm_data((const char**)fin->mThumbnailPixmap);

			if ((!fin->mIsAlternate) || ((fin->mIsAlternate) && mShowAlternates))
			{
				gchar *idCode, *name, *damage, *date, *location;

				if (mOptions->mHideIDs) //***1.65 - hide IDs if needed
				{
					idCode = new gchar[5];
					strcpy(idCode, "****");
				}
				else if ("NONE" == fin->mIDCode)
					idCode = NULL;
				else
				{
					idCode = new gchar[fin->mIDCode.length() + 1];
					strcpy(idCode, fin->mIDCode.c_str());
				}

				if ("NONE" == fin->mName)
					name = NULL;
				else {
					name = new gchar[fin->mName.length() + 1];
					strcpy(name, fin->mName.c_str());
				}

				if ("NONE" == fin->mDamageCategory) {
					damage = new gchar[12];
					strcpy(damage, "Unspecified");
				}
				else {
					damage = new gchar[fin->mDamageCategory.length() + 1];
					strcpy(damage, fin->mDamageCategory.c_str());
				}

				if ("NONE" == fin->mDateOfSighting)
					date = NULL;
				else {
					date = new gchar[fin->mDateOfSighting.length() + 1];
					strcpy(date, fin->mDateOfSighting.c_str());
				}

				if ("NONE" == fin->mLocationCode)
					location = NULL;
				else {
					location = new gchar[fin->mLocationCode.length() + 1];
					strcpy(location, fin->mLocationCode.c_str());
				}

				// Get position of appended record.
 				gtk_tree_store_append(mStore, &iter, NULL);

				gtk_tree_store_set(mStore, &iter,
					PIXBUFF_COLUMN, pixbuf,
					INDV_ID_COLUMN, fin->mDataPos,
					USER_SUPPLIED_ID_COLUMN, idCode,
					NAME_COLUMN, name,
					DAMAGE_COLUMN, damage,
					DATE_COLUMN, date,
					LOCATION_COLUMN, location,
					-1);

				delete[] idCode;
				delete[] name;
				delete[] damage;
				delete[] date;
				delete[] location;

			} //***1.95 - end of restriction on list

			delete fin;
			i++;

#ifdef _DEBUG
			if (i == 1000)
				break;
#endif // !_DEBUG

		}

		cout << endl << "Set the tree sort order." << endl;
		if (sort_column_id < 0)
			sort_column_id = mNewSort;

		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(mModel), sort_column_id, sort_order);

		cout << endl << "Re-attach model to view." << endl;
		gtk_tree_view_set_model(GTK_TREE_VIEW(mView), GTK_TREE_MODEL(mStore));
		g_object_unref(mStore);

		cout << endl << "Move to first row in list." << endl;
		if (gtk_tree_model_get_iter_first(mModel, &iter)) {
			path = gtk_tree_model_get_path(mModel, &iter);

			cout << endl << "Select our record." << endl;
			gtk_tree_selection_select_path(mSelection, path);
		}
		
		cout << endl << "Resize all columns to their optimal width." << endl;
		gtk_tree_view_columns_autosize(GTK_TREE_VIEW(mView));

		cout << endl << "List creation complete" << endl;
		time(&endtime);
		seconds = difftime(endtime, starttime);

		printf("Total time required to load data: %.f seconds\n", seconds);

	}
	catch (Error e) {
		showError(_("The database seems to be corrupted.\n"
			"Some (or all) entries may not appear\n"
			"correctly."));
	}
}

//*******************************************************************
// Refresh the display by adding single fin to mainWindow.
//          
//
void MainWindow::refreshDatabaseDisplay(unsigned long id)
{
	try {
		/* Returns the number of rows in the model. We could use mIndividualsTblRowCnt, which should give us the same number.  */
		gint n_rows = gtk_tree_model_iter_n_children(mModel, NULL);

		if (n_rows == 0){
			gtk_widget_set_sensitive(mButtonNext, FALSE);
			gtk_widget_set_sensitive(mButtonPrev, FALSE);
			gtk_widget_set_sensitive(mButtonModify, FALSE);

			// we may be loading an empty database so make sure leftovers
			// from previous database are freed
			if (NULL != mSelectedFin)
			{
				delete mSelectedFin;
				mSelectedFin = NULL;
			}
			if (NULL != mImage)
			{
				delete mImage;
				mImage = NULL;
			}
		}					//***002DB <
		else
			gtk_widget_set_sensitive(mButtonModify, TRUE); //**003MR

		//***1.85 - set font as currently selected 
		gtk_widget_modify_font(
			mView,
			(pango_font_description_from_string(mOptions->mCurrentFontName.c_str())));

		// Some variables for the pixmap display
		GdkPixbuf *pixbuf = NULL;

		// Some variables for our TreeView.
		GtkTreeIter iter;
		gint sort_column_id, *selection_pos(0);
		GtkSortType sort_order;
		GtkTreePath *path;

		mSelection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mView));

		/* Speed Issues when Adding a Lot of Rows
		A common scenario is that a model needs to be filled with a lot of rows at some point,
		either at start-up, or when some file is opened. An equally common scenario is that this
		takes an awfully long time even on powerful machines once the model contains more than
		a couple of thousand rows, with an exponentially decreasing rate of insertion.
		Writing a custom model might be the best thing to do in this case.
		Nevertheless, there are some things you can do to work around this problem and speed things
		up a bit even with the stock Gtk+ models:

		Firstly, you should detach your list store or tree store from the tree view before doing
		your mass insertions, then do your insertions, and only connect your store to the tree view
		again when you are done with your insertions.

		Secondly, you should make sure that sorting is disabled while you are doing your mass insertions,
		otherwise your store might be resorted after each and every single row insertion, which is going
		to be everything but fast.

		Thirdly, you should not keep around a lot of tree row references if you have so many rows,
		because with each insertion (or removal) every single tree row reference will check whether its
		path needs to be updated or not.
		*/
		/* Get the model. */
		mModel = gtk_tree_view_get_model(GTK_TREE_VIEW(mView));
		/* Get the store. */
		mStore = GTK_TREE_STORE(mModel);
		// Store current sorting information and store in &sort_column_id, &sort_order.
		gtk_tree_sortable_get_sort_column_id(GTK_TREE_SORTABLE(mModel), &sort_column_id, &sort_order);
		// Set sort order to unsorted.
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(mModel), GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID,
			GTK_SORT_ASCENDING);

		// No more than one item can be selected
		gtk_tree_selection_set_mode(mSelection, GTK_SELECTION_SINGLE);
		/* Make sure the model stays with us after the tree view unrefs it */
		g_object_ref(mStore);
		/* Detach model from view */
		gtk_tree_view_set_model(GTK_TREE_VIEW(mView), NULL);

		/* Add this row to the store. */

		DatabaseFin<ColorImage> *fin = mDatabase->getItemAbsolute(id);

		/*********************************************************/
		/* *** FYI: fin->mDataPos is actually individuals.id *** */
		/*********************************************************/

		/* gdk_pixmap_colormap_create_from_xpm_d has been deprecated since version 2.22 and should not be used in newly-written code. Use a GdkPixbuf instead. */
		pixbuf = gdk_pixbuf_new_from_xpm_data((const char**)fin->mThumbnailPixmap);

		if ((!fin->mIsAlternate) || ((fin->mIsAlternate) && mShowAlternates))
		{
			gchar *idCode, *name, *damage, *date, *location;

			if (mOptions->mHideIDs) //***1.65 - hide IDs if needed
			{
				idCode = new gchar[5];
				strcpy(idCode, "****");
			}
			else if ("NONE" == fin->mIDCode)
				idCode = NULL;
			else
			{
				idCode = new gchar[fin->mIDCode.length() + 1];
				strcpy(idCode, fin->mIDCode.c_str());
			}

			if ("NONE" == fin->mName)
				name = NULL;
			else {
				name = new gchar[fin->mName.length() + 1];
				strcpy(name, fin->mName.c_str());
			}

			if ("NONE" == fin->mDamageCategory) {
				damage = new gchar[12];
				strcpy(damage, "Unspecified");
			}
			else {
				damage = new gchar[fin->mDamageCategory.length() + 1];
				strcpy(damage, fin->mDamageCategory.c_str());
			}

			if ("NONE" == fin->mDateOfSighting)
				date = NULL;
			else {
				date = new gchar[fin->mDateOfSighting.length() + 1];
				strcpy(date, fin->mDateOfSighting.c_str());
			}

			if ("NONE" == fin->mLocationCode)
				location = NULL;
			else {
				location = new gchar[fin->mLocationCode.length() + 1];
				strcpy(location, fin->mLocationCode.c_str());
			}

			// Get position of appended record.
			gtk_tree_store_append(mStore, &iter, NULL);

			gtk_tree_store_set(mStore, &iter,
				PIXBUFF_COLUMN, pixbuf,
				INDV_ID_COLUMN, fin->mDataPos,
				USER_SUPPLIED_ID_COLUMN, idCode,
				NAME_COLUMN, name,
				DAMAGE_COLUMN, damage,
				DATE_COLUMN, date,
				LOCATION_COLUMN, location,
				-1);

			/* Keep track of how many rows are in our model. */
			mIndividualsTblRowCnt = n_rows + 1;
			cout << endl << "New Record Count: " << mIndividualsTblRowCnt << endl;
			
			delete[] idCode;
			delete[] name;
			delete[] damage;
			delete[] date;
			delete[] location;

		} //***1.95 - end of restriction on list

		/* In case the on_mainTreeView_select_row doesn't fire.*/
		mSelectedFin = mDatabase->getItemAbsolute(id);
		delete fin;

		cout << endl << "Set the tree sort order." << endl;
		if (sort_column_id < 0)
			sort_column_id = mNewSort;

		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(mModel), sort_column_id, sort_order);

		cout << endl << "Re-attach model to view." << endl;
		gtk_tree_view_set_model(GTK_TREE_VIEW(mView), GTK_TREE_MODEL(mStore));
		g_object_unref(mStore);

		cout << endl << "Resize all columns to their optimal width." << endl;
		gtk_tree_view_columns_autosize(GTK_TREE_VIEW(mView));

		// Get path and selection position.
		path = gtk_tree_model_get_path(mModel, &iter);
		selection_pos = gtk_tree_path_get_indices(path);

		/* Select our record to force initialization of selection variables.*/
		gtk_tree_selection_select_path(mSelection, path);
		cout << "Successfully updated MainWindow listing." << endl;

		/* Update the status message on the main window. */
		char numEntriesStr[64];
		sprintf(numEntriesStr, "Ready: %d fins loaded", mIndividualsTblRowCnt);
		displayStatusMessage(numEntriesStr);
	}
	catch (Error e) {
		showError(_("The database seems to be corrupted.\n"
			"Some (or all) entries may not appear\n"
			"correctly."));
	}
}

//*******************************************************************
//
// Function simply adjusts the scrolling window so that the selected fin
// is visible.  Size of the model is indicated by the number of rows in the model.
//
void MainWindow::setScrollWindowPosition(int newCurEntry)       //***004CL
{
	/* Returns the number of rows in the model. */
	gint n_rows = gtk_tree_model_iter_n_children(mModel, NULL);

	if (n_rows == 0)
		return;

	// else force scrollable window to scroll down to selected row

	const int lineHeight = DATABASEFIN_THUMB_HEIGHT + 2;
	static int lastEntry = 0;

	int pageEntries = mScrollable->allocation.height / lineHeight;

	GtkAdjustment *adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(mScrollable));
	int topEntry = (int)(adj->value) / lineHeight;

	//g_print("top/last/this = (%d %d %d)\n", topEntry, lastEntry,newCurEntry);

	if ((newCurEntry > topEntry + pageEntries - 3) || (newCurEntry < topEntry))
	{
		// NOTE: the (-2) in the test above prevents highlighting of partially visible
		// item at bottom of page when pressing NEXT button

		if ((lastEntry + 1 == newCurEntry) &&
			(newCurEntry > topEntry) && (newCurEntry == topEntry + pageEntries - 2))
			adj->value += lineHeight; // just scroll down one line
		else
		{
			/* Rows that had their ID Code changed were showing up hidden at the top of the window.  Correction to follow. */
			if (newCurEntry > 1)
				newCurEntry = newCurEntry - 1;

			// reposition list so newCurEntry is at top
			float where = (double)newCurEntry / n_rows; //***1.96
			adj->value = where * (adj->upper - adj->lower);
			topEntry = newCurEntry;
		}

		// this should be called but seems to be meaningless and scroll update
		// works without it - JHS
		//gtk_scrolled_window_set_vadjustment(GTK_SCROLLED_WINDOW(mScrollable),adj);

		gtk_adjustment_value_changed(adj);
	}
	lastEntry = newCurEntry;
}

//*******************************************************************
//***SAH: This function is called on each expose event for the notebook pages with Outline,Image,and OrigImage
//        It is also called once in mainWindowCreate()
void MainWindow::updateCursor()
{
	GdkBitmap *bitmap, *mask;
	GdkColor white = { 0, 0xFFFF, 0xFFFF, 0xFFFF };
	GdkColor black = { 0, 0x0000, 0x0000, 0x0000 };

	//if (NULL != mCursor)
	//gdk_cursor_destroy(mCursor);

	if (mCursor == NULL){
		bitmap = gdk_bitmap_create_from_data(NULL,
			magnify_cursor, magnify_cursor_width,
			magnify_cursor_height);
		mask = gdk_bitmap_create_from_data(NULL,
			magnify_mask, magnify_cursor_width,
			magnify_cursor_height);
		mCursor = gdk_cursor_new_from_pixmap(
			bitmap, mask, &black, &white,
			magnify_xhot, magnify_yhot);
	}

	// I'm paranoid, what can I say?
	if (NULL != mCursor && NULL != mDrawingAreaOutline &&
		NULL != mDrawingAreaOutline->window) //***1.99 - notebook page may be hidden
		gdk_window_set_cursor(mDrawingAreaOutline->window, mCursor);

	if (NULL != mCursor && NULL != mDrawingAreaImage &&
		NULL != mDrawingAreaImage->window) //***1.99 - notebook page may be hidden
		gdk_window_set_cursor(mDrawingAreaImage->window, mCursor);

	if (NULL != mCursor && NULL != mDrawingAreaOrigImage &&
		NULL != mDrawingAreaOrigImage->window) //***1.99 - notebook page may be hidden
		gdk_window_set_cursor(mDrawingAreaOrigImage->window, mCursor);

}

//*******************************************************************
void MainWindow::updateGC()
{
	if (NULL == mGC) {
		if (NULL == mDrawingAreaOutline ||
			NULL == mDrawingAreaOutline->window) //***1.99 - notebook page may  be hidden
			return;

		mGC = gdk_gc_new(mDrawingAreaOutline->window);
	}

	updateGCColor();
}

//*******************************************************************
void MainWindow::updateGCColor()
{
	if (NULL == mGC)
		return;

	GdkColormap *colormap;
	GdkColor gdkColor;

	//gdkColor.red = (gushort)(0xFFFFF * mOptions->mCurrentColor[0]);
	//gdkColor.green = (gushort)(0xFFFFF * mOptions->mCurrentColor[1]);
	//gdkColor.blue = (gushort)(0xFFFFF * mOptions->mCurrentColor[2]);

	//***1.7 - use BLACK now
	gdkColor.red = 0;
	gdkColor.green = 0;
	gdkColor.blue = 0;

	colormap = gdk_colormap_get_system();
	gdk_color_alloc(colormap, &gdkColor);

	gdk_gc_set_foreground(mGC, &gdkColor);
}

//*******************************************************************
void MainWindow::refreshOptions(bool initialShow, bool forceReload)
{
	updateGCColor();
	if (!initialShow)
		refreshOutline();

	switch (mOptions->mToolbarDisplay) {
	case TEXT:
		gtk_toolbar_set_style(
			GTK_TOOLBAR(mToolBar),
			GTK_TOOLBAR_TEXT);
		break;
	case PICTURES:
		gtk_toolbar_set_style(
			GTK_TOOLBAR(mToolBar),
			GTK_TOOLBAR_ICONS);
		break;
	default:
		gtk_toolbar_set_style(
			GTK_TOOLBAR(mToolBar),
			GTK_TOOLBAR_BOTH);
		break;
	}

	//***1.96a - reload if ID show/hide state changes
	if (forceReload)
	{
		refreshDisplayFromDB();
	}

	//***1.96a - need to force "new" font to be displayed
	gtk_widget_modify_font(
		mView,
		(pango_font_description_from_string(mOptions->mCurrentFontName.c_str())));
}

//*******************************************************************
void MainWindow::displayStatusMessage(const string &msg)
{
	gtk_statusbar_pop(GTK_STATUSBAR(mStatusBar), mContextID);
	gtk_statusbar_push(GTK_STATUSBAR(mStatusBar), mContextID, msg.c_str());
}

//*******************************************************************
GtkWidget *MainWindow::getWindow()
{
	return mWindow;
}

//*******************************************************************
GtkWidget* MainWindow::createMainWindow(toolbarDisplayType toolbarDisplay)
{
	GtkWidget *mainWindow;
	GtkWidget *mainVBox;
	GtkWidget *mainMenuHandleBox;
	GtkWidget *manMenuBar;
	//guint tmp_key;
	GtkWidget *file;
	GtkWidget *file_menu;
	GtkWidget *open;
	GtkWidget *matching_queue;
	GtkWidget *separator1;
	GtkWidget *exit;
	GtkWidget *catalog;        //***1.4
	GtkWidget *catalog_menu;   //***1.4
	GtkWidget *catalog_new;    //***1.4
	GtkWidget *catalog_view;   //***1.4
	GtkWidget *catalog_select; //***1.4
	GtkWidget *settings;
	GtkWidget *settings_menu;
	GtkWidget *options;
	GtkWidget *help;
	GtkWidget *help_menu;
	GtkWidget *about;
	GtkWidget *mainToolbarHandleBox;
	GtkWidget *tmp_toolbar_icon;
	GtkWidget *mainButtonOpen;
	GtkWidget *mainButtonOpenTrace;  //***1.5
	GtkWidget *mainButtonMatchingQueue;
	//GtkWidget *mainButtonOptions;
	GtkWidget *mainButtonExit;
	GtkWidget *mainHPaned;
	GtkWidget *mainLeftVBox;
	GtkWidget *mainScrolledWindow;

	GtkWidget *hbuttonbox1;
	//guint mainButtonPrev_key;
	//guint mainButtonNext_key;
	//guint mainButtonModify_key;		//***002DB
	GtkWidget *mainInfoTable;
	GtkWidget *mainLabelID;
	GtkWidget *mainLabelName;
	GtkWidget *mainLabelDate;
	GtkWidget *mainLabelRoll;
	GtkWidget *mainLabelLocation;
	GtkWidget *mainLabelDamage;
	GtkWidget *mainLabelDescription;
	GtkWidget *mainRightVBox;
	GtkWidget *mainEventBoxImage;
	GtkWidget *mainFrameOutline;
	GtkWidget *mainEventBoxOutline;
	GtkAccelGroup *accel_group;
	GtkTooltips *tooltips;
	GtkWidget *tearoff;
	GtkWidget *tmpLabel, *tmpIcon, *tmpBox;
	GtkWidget *leftFrame, *rightFrame;


	string MainWinTitle = "DARWIN - ";
	//***1.85 - set the database filename / message for the window
	switch (mDatabase->status())
	{
	case Database::loaded:
		MainWinTitle += mOptions->mDatabaseFileName;
		break;
	case Database::fileNotFound:
		MainWinTitle += "Database file not found";
		break;
	case Database::errorCreating:
		MainWinTitle += "Database file could not be created";
		break;
	case Database::errorLoading:
		MainWinTitle += "Error loading database file";
		break;
	case Database::oldDBVersion:
		MainWinTitle += "OLD database file version - not supported";
		break;
	default:
		MainWinTitle += "unknown error with database file";
		break;
	}

	tooltips = gtk_tooltips_new();

	accel_group = gtk_accel_group_new();

	mainWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	g_object_set_data(G_OBJECT(mainWindow), "mainWindow", mainWindow);
	gtk_window_set_title(GTK_WINDOW(mainWindow), _(MainWinTitle.c_str())); //***1.85
	gtk_window_set_wmclass(GTK_WINDOW(mainWindow), "darwin_main", "DARWIN");
	gtk_window_set_position(GTK_WINDOW(mainWindow), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(mainWindow), 800, 600); //***1.7

	mainVBox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(mainVBox);
	gtk_container_add(GTK_CONTAINER(mainWindow), mainVBox);

	mainMenuHandleBox = gtk_handle_box_new();
	gtk_widget_show(mainMenuHandleBox);
	gtk_box_pack_start(GTK_BOX(mainVBox), mainMenuHandleBox, FALSE, TRUE, 0);

	manMenuBar = gtk_menu_bar_new();
	gtk_widget_show(manMenuBar);
	gtk_container_add(GTK_CONTAINER(mainMenuHandleBox), manMenuBar);

	// create "File" menu item (mnemonic ALT-F)

	file = gtk_menu_item_new_with_mnemonic(_("_File"));
	gtk_widget_show(file);
	gtk_container_add(GTK_CONTAINER(manMenuBar), file);

	file_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(file), file_menu);

	// allow File menu to be detachable

	tearoff = gtk_tearoff_menu_item_new();
	gtk_menu_append(GTK_MENU(file_menu), tearoff);
	gtk_widget_show(tearoff);

	// create "New Database" item in File submenu (mnemonic N, accelerator Ctrl-N)

	GtkWidget *newdb = gtk_menu_item_new();
	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new_with_mnemonic(_("    _New Database       Ctrl-N"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), newdb);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(newdb), tmpBox);

	gtk_widget_show(newdb);
	gtk_container_add(GTK_CONTAINER(file_menu), newdb);
	gtk_tooltips_set_tip(tooltips, newdb, _("Create a new empty database using the current default\n"
		"catalog scheme."), NULL);
	gtk_widget_add_accelerator(newdb, "activate", accel_group,
		GDK_N, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	// create a separator line in submenu

	separator1 = gtk_menu_item_new();
	gtk_widget_show(separator1);
	gtk_container_add(GTK_CONTAINER(file_menu), separator1);
	gtk_widget_set_sensitive(separator1, FALSE);

	// create "Open Image" menu item in File submenu (mnemonic O, accelerator Ctrl-O)

	open = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, open_image_small_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_Open Image          Ctrl-O"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), open);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(open), tmpBox);

	gtk_widget_show(open);
	gtk_container_add(GTK_CONTAINER(file_menu), open);
	gtk_tooltips_set_tip(tooltips, open, _("Open a dorsal fin image."), NULL);
	gtk_widget_add_accelerator(open, "activate", accel_group,
		GDK_O, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	//***1.85 - if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(open, FALSE);
	mOpenImageMenuItem = open;

	// create "Open Traced Fin" menu item in File submenu (mnemonic T, accelerator Ctrl-T)

	GtkWidget *openTFin = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, open_trace_small_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("Open _Traced Fin    Ctrl-T"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), openTFin);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(openTFin), tmpBox);

	gtk_widget_show(openTFin);
	gtk_container_add(GTK_CONTAINER(file_menu), openTFin);
	gtk_tooltips_set_tip(tooltips, openTFin, _("Open a previously saved dorsal fin tracing."), NULL);
	gtk_widget_add_accelerator(openTFin, "activate", accel_group,
		GDK_T, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	//***1.85 - if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(openTFin, FALSE);
	mOpenFinMenuItem = openTFin;

	// create "Open Database" item in File submenu (mnemonic D, accelerator Ctrl-D)

	GtkWidget *opendb = gtk_menu_item_new();
	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new_with_mnemonic(_("    Open _Database      Ctrl-D"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), opendb);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(opendb), tmpBox);

	gtk_widget_show(opendb);
	gtk_container_add(GTK_CONTAINER(file_menu), opendb);
	gtk_tooltips_set_tip(tooltips, opendb, _("Open a different database."), NULL);
	gtk_widget_add_accelerator(opendb, "activate", accel_group,
		GDK_D, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	// create a separator line in submenu

	separator1 = gtk_menu_item_new();
	gtk_widget_show(separator1);
	gtk_container_add(GTK_CONTAINER(file_menu), separator1);
	gtk_widget_set_sensitive(separator1, FALSE);

	// create "Matching Queue" item in File submenu (mnemonic Q, accelerator Ctrl-Q)

	matching_queue = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, matching_queue_small_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("Matching _Queue     Ctrl-Q"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), matching_queue);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(matching_queue), tmpBox);

	gtk_widget_show(matching_queue);
	gtk_container_add(GTK_CONTAINER(file_menu), matching_queue);
	gtk_tooltips_set_tip(tooltips, matching_queue,
		_("Create queues of saved fin traces, run queued matches in batch mode, and review saved match results."),
		NULL);
	gtk_widget_add_accelerator(matching_queue, "activate", accel_group,
		GDK_Q, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	//***1.85 - if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(matching_queue, FALSE);
	mQueueMenuItem = matching_queue;

	// create a separator line in submenu

	separator1 = gtk_menu_item_new();
	gtk_widget_show(separator1);
	gtk_container_add(GTK_CONTAINER(file_menu), separator1);
	gtk_widget_set_sensitive(separator1, FALSE);

	//***1.85 - menu options for backup and restore

	GtkWidget *backup = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new_with_mnemonic(_("    _Backup                  Ctrl-B"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), backup);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(backup), tmpBox);

	gtk_widget_show(backup);
	gtk_container_add(GTK_CONTAINER(file_menu), backup);
	gtk_tooltips_set_tip(tooltips, backup, _("Backup the currently open database."), NULL);
	gtk_widget_add_accelerator(backup, "activate", accel_group,
		GDK_B, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	//***1.85 - if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(backup, FALSE);
	mBackupMenuItem = backup;

	GtkWidget *restore = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new_with_mnemonic(_("    _Restore                 Ctrl-R"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), restore);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(restore), tmpBox);

	gtk_widget_show(restore);
	gtk_container_add(GTK_CONTAINER(file_menu), restore);
	gtk_tooltips_set_tip(tooltips, restore, _("Restore a database from previous backup."), NULL);
	gtk_widget_add_accelerator(restore, "activate", accel_group,
		GDK_R, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);


	// create a separator line in submenu

	separator1 = gtk_menu_item_new();
	gtk_widget_show(separator1);
	gtk_container_add(GTK_CONTAINER(file_menu), separator1);
	gtk_widget_set_sensitive(separator1, FALSE);

	////////////////////////

	//***1.85 - menu options for import and export

	/*GtkWidget *importDB = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new(_("    Import"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), importDB);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(importDB), tmpBox);

	gtk_widget_show (importDB);
	gtk_container_add (GTK_CONTAINER (file_menu), importDB);
	gtk_tooltips_set_tip (tooltips, importDB,
	_("Import a database ...\n"
	"(into a NEW user specified Survey Area.)"), NULL);*/


	// new Import submenu

	GtkWidget *import = gtk_menu_item_new_with_mnemonic(_("    Import"));
	gtk_widget_show(import);
	gtk_container_add(GTK_CONTAINER(file_menu), import);

	GtkWidget *importSub = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(import), importSub);

	// Import Catalog submenu item

	GtkWidget *importDB = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new(_("    Import Catalog"));

	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), importDB);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(importDB), tmpBox);

	gtk_widget_show(importDB);

	gtk_container_add(GTK_CONTAINER(importSub), importDB);

	gtk_tooltips_set_tip(tooltips, importDB,
		_("Import a database ...\n"
		"(into a NEW user specified Survey Area.)"), NULL);

	mImportDBMenuItem = importDB;

	// Import Fin (*.finz) submenu item

	mImportFinzMenuItem = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new(_("    Import Fin (*.finz)"));

	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mImportFinzMenuItem);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(mImportFinzMenuItem), tmpBox);

	gtk_widget_show(mImportFinzMenuItem);

	gtk_container_add(GTK_CONTAINER(importSub), mImportFinzMenuItem);

	gtk_tooltips_set_tip(tooltips, mImportFinzMenuItem,
		_("Import one or more fins ...\n"
		"(from user specified *.finz files)"), NULL);

	//***1.85 - if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(mImportFinzMenuItem, FALSE);

	//***************************************************************//

	// new Export submenu

	mExportSubMenuItem = gtk_menu_item_new_with_mnemonic(_("    Export"));
	gtk_widget_show(mExportSubMenuItem);
	gtk_container_add(GTK_CONTAINER(file_menu), mExportSubMenuItem);

	// if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(mExportSubMenuItem, FALSE);

	GtkWidget *exportSubMenu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(mExportSubMenuItem), exportSubMenu);

	// Export Catalog submenu item

	mExportDBMenuItem = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new(_("    Export Catalog"));

	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mExportDBMenuItem);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(mExportDBMenuItem), tmpBox);

	gtk_widget_show(mExportDBMenuItem);

	gtk_container_add(GTK_CONTAINER(exportSubMenu), mExportDBMenuItem);

	gtk_tooltips_set_tip(tooltips, mExportDBMenuItem,
		_("Export the currently open database ...\n"
		"(to a user specified *.zip file)"), NULL);

	// Export Fin (*.finz) submenu item

	mExportFinzMenuItem = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new(_("    Export Fin (*.finz)"));

	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mExportFinzMenuItem);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(mExportFinzMenuItem), tmpBox);

	gtk_widget_show(mExportFinzMenuItem);

	gtk_container_add(GTK_CONTAINER(exportSubMenu), mExportFinzMenuItem);

	gtk_tooltips_set_tip(tooltips, mExportFinzMenuItem,
		_("Export a fin ...\n"
		"(to a user specified *.finz file)"), NULL);

	//***2.02 - new menu item to allow generation of FULL-SIZE modified images

	// Export FullSzImgs (*FullSize.png) submenu item

	mExportFullSzImgsMenuItem = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpLabel = gtk_label_new(_("    Export Images (Full-Size)"));

	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mExportFullSzImgsMenuItem);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(mExportFullSzImgsMenuItem), tmpBox);

	gtk_widget_show(mExportFullSzImgsMenuItem);

	gtk_container_add(GTK_CONTAINER(exportSubMenu), mExportFullSzImgsMenuItem);

	gtk_tooltips_set_tip(tooltips, mExportFullSzImgsMenuItem,
		_("Export Modified Images from Catalog ...\n"
		"(to a user specified location)"), NULL);

	// create a separator line in submenu

	separator1 = gtk_menu_item_new();
	gtk_widget_show(separator1);
	gtk_container_add(GTK_CONTAINER(file_menu), separator1);
	gtk_widget_set_sensitive(separator1, FALSE);

	////////////////////////
	// create "Exit" menu item in File submenu (mnemonic X, accelerator Ctrl-X)

	exit = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, exit_small_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("E_xit                      Ctrl-X"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), exit);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(exit), tmpBox);

	gtk_widget_show(exit);
	gtk_container_add(GTK_CONTAINER(file_menu), exit);
	gtk_tooltips_set_tip(tooltips, exit, _("Exit the DARWIN program."), NULL);
	gtk_widget_add_accelerator(exit, "activate", accel_group,
		GDK_X, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	// create "Settings" menu item (mnemonic ALT-S)

	settings = gtk_menu_item_new_with_mnemonic(_("_Settings"));
	gtk_widget_show(settings);
	gtk_container_add(GTK_CONTAINER(manMenuBar), settings);

	settings_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(settings), settings_menu);

	// allow Settings submenu to be detachable

	tearoff = gtk_tearoff_menu_item_new();
	gtk_menu_append(GTK_MENU(settings_menu), tearoff);
	gtk_widget_show(tearoff);

	// create "Options" menu item in Settings submenu (mnemonic P, accelerator Ctrl-P)

	options = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, options_small_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("O_ptions        Ctrl-P"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), options);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(options), tmpBox);

	gtk_widget_show(options);
	gtk_container_add(GTK_CONTAINER(settings_menu), options);
	gtk_tooltips_set_tip(tooltips, options,
		_("Settings that change the way DARWIN operates."),
		NULL);
	gtk_widget_add_accelerator(options, "activate", accel_group,
		GDK_P, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	//***1.4 - create "Catalog Scheme" menu item

	catalog = gtk_menu_item_new_with_mnemonic(_("_Catalog Scheme"));
	gtk_widget_show(catalog);
	gtk_container_add(GTK_CONTAINER(settings_menu), catalog);

	catalog_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(catalog), catalog_menu);

	// allow Catalog submenu to be detachable

	//tearoff = gtk_tearoff_menu_item_new();
	//gtk_menu_append(GTK_MENU(catalog_menu), tearoff);
	//gtk_widget_show(tearoff);


	// create "Define New" menu item in Catalog Scheme submenu

	catalog_new = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	//tmpIcon = create_pixmap_from_data(tmpBox, options_small_xpm);
	//gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	//gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("Define New _Scheme     Ctrl-S"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), catalog_new);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(catalog_new), tmpBox);

	gtk_widget_show(catalog_new);
	gtk_container_add(GTK_CONTAINER(catalog_menu), catalog_new);
	gtk_tooltips_set_tip(tooltips, catalog_new,
		_("Define the details of a new Catalog Scheme."),
		NULL);
	gtk_widget_add_accelerator(catalog_new, "activate", accel_group,
		GDK_S, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	// create "View/Edit" menu item in Catalog Scheme submenu

	catalog_view = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	//tmpIcon = create_pixmap_from_data(tmpBox, options_small_xpm);
	//gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	//gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_View/Edit                    Ctrl-V"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), catalog_view);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(catalog_view), tmpBox);

	gtk_widget_show(catalog_view);
	gtk_container_add(GTK_CONTAINER(catalog_menu), catalog_view);
	gtk_tooltips_set_tip(tooltips, catalog_view,
		_("View or Edit the details of all defined Catalog Schemes."),
		NULL);
	gtk_widget_add_accelerator(catalog_view, "activate", accel_group,
		GDK_V, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);

	// create "Select Active" menu item in Catalog Scheme submenu

	catalog_select = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	//tmpIcon = create_pixmap_from_data(tmpBox, options_small_xpm);
	//gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	//gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("Set Default                  Ctrl-A"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), catalog_select);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(catalog_select), tmpBox);

	gtk_widget_show(catalog_select);
	gtk_container_add(GTK_CONTAINER(catalog_menu), catalog_select);
	gtk_tooltips_set_tip(tooltips, catalog_select,
		_("Set the Default Catalog Scheme\n"
		"for any NEW database created."),
		NULL);
	gtk_widget_add_accelerator(catalog_select, "activate", accel_group,
		GDK_A, GDK_CONTROL_MASK,
		GTK_ACCEL_VISIBLE);


	//***1.9 - create "Data" menu item (mnemonic ALT-T)

	GtkWidget *data = gtk_menu_item_new_with_mnemonic(_("Da_ta"));
	gtk_widget_show(data);
	gtk_container_add(GTK_CONTAINER(manMenuBar), data);

	GtkWidget *data_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(data), data_menu);

	GtkWidget *exportData = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, magnify_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("Export Selected Da_ta"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), exportData);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(exportData), tmpBox);

	gtk_widget_show(exportData);
	gtk_container_add(GTK_CONTAINER(data_menu), exportData);
	gtk_tooltips_set_tip(tooltips, exportData, _("Export selected data in <tab> separated format."), NULL);

	// create "Help" menu item (mnemonic ALT-H)

	help = gtk_menu_item_new_with_mnemonic(_("_Help"));
	gtk_menu_item_right_justify(GTK_MENU_ITEM(help));
	gtk_widget_show(help);
	gtk_container_add(GTK_CONTAINER(manMenuBar), help);

	help_menu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(help), help_menu);

	// allow Help submenu to be detachable

	tearoff = gtk_tearoff_menu_item_new();
	gtk_menu_append(GTK_MENU(help_menu), tearoff);
	gtk_widget_show(tearoff);

	// create "Documentation" menu item in Help Submenu (mnemonic D)

	GtkWidget *docs = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, about_small_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_Documentation"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), docs);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(docs), tmpBox);

	gtk_widget_show(docs);
	gtk_container_add(GTK_CONTAINER(help_menu), docs);
	gtk_tooltips_set_tip(tooltips, docs, _("DARWIN Online Documentation."), NULL);

	// create "About" menu item in Help Submenu (mnemonic A)

	about = gtk_menu_item_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, about_small_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_About"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), about);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, FALSE, FALSE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(about), tmpBox);

	gtk_widget_show(about);
	gtk_container_add(GTK_CONTAINER(help_menu), about);
	gtk_tooltips_set_tip(tooltips, about, _("About DARWIN."), NULL);

	// create a toolbar with commonly used buttons

	mainToolbarHandleBox = gtk_handle_box_new();
	gtk_widget_show(mainToolbarHandleBox);
	gtk_box_pack_start(GTK_BOX(mainVBox), mainToolbarHandleBox, FALSE, FALSE, 0);
	gtk_handle_box_set_shadow_type(GTK_HANDLE_BOX(mainToolbarHandleBox), GTK_SHADOW_NONE);

	switch (toolbarDisplay) {
	case TEXT:
		mToolBar = gtk_toolbar_new();
		gtk_toolbar_set_orientation(GTK_TOOLBAR(mToolBar), GTK_ORIENTATION_HORIZONTAL);
		gtk_toolbar_set_style(GTK_TOOLBAR(mToolBar), GTK_TOOLBAR_TEXT);
		break;
	case PICTURES:
		mToolBar = gtk_toolbar_new();
		gtk_toolbar_set_orientation(GTK_TOOLBAR(mToolBar), GTK_ORIENTATION_HORIZONTAL);
		gtk_toolbar_set_style(GTK_TOOLBAR(mToolBar), GTK_TOOLBAR_ICONS);
		break;
	default:
		mToolBar = gtk_toolbar_new();
		gtk_toolbar_set_orientation(GTK_TOOLBAR(mToolBar), GTK_ORIENTATION_HORIZONTAL);
		gtk_toolbar_set_style(GTK_TOOLBAR(mToolBar), GTK_TOOLBAR_BOTH);
		break;
	}

	gtk_widget_show(mToolBar);
	gtk_container_add(GTK_CONTAINER(mainToolbarHandleBox), mToolBar);

	// create "Open" button (shortcut to Open File Dialog - opening a fin image)

	tmp_toolbar_icon = create_pixmap_from_data(mainWindow, open_image_xpm);
	mainButtonOpen = gtk_toolbar_append_element(GTK_TOOLBAR(mToolBar),
		GTK_TOOLBAR_CHILD_BUTTON,
		NULL,
		_("Open"),
		_("Open a dorsal fin image."), NULL,
		tmp_toolbar_icon, NULL, NULL);
	gtk_button_set_relief(GTK_BUTTON(mainButtonOpen), GTK_RELIEF_NONE);
	gtk_widget_show(mainButtonOpen);

	//***1.85 - if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(mainButtonOpen, FALSE);
	mOpenImageButton = mainButtonOpen;

	// create another "Open" button (shortcut to Open File Chooser Dialog - opening a fin trace)

	tmp_toolbar_icon = create_pixmap_from_data(mainWindow, open_trace_xpm);
	mainButtonOpenTrace = gtk_toolbar_append_element(GTK_TOOLBAR(mToolBar),
		GTK_TOOLBAR_CHILD_BUTTON,
		NULL,
		_("Open"),
		_("Open a previously saved dorsal fin tracing."), NULL,
		tmp_toolbar_icon, NULL, NULL);
	gtk_button_set_relief(GTK_BUTTON(mainButtonOpenTrace), GTK_RELIEF_NONE);
	gtk_widget_show(mainButtonOpenTrace);

	//***1.85 - if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(mainButtonOpenTrace, FALSE);
	mOpenFinButton = mainButtonOpenTrace;

	// create "Queue" button (shortcut to Matching Queue Dialog)

	tmp_toolbar_icon = create_pixmap_from_data(mainWindow, matching_queue_xpm);
	mainButtonMatchingQueue = gtk_toolbar_append_element(GTK_TOOLBAR(mToolBar),
		GTK_TOOLBAR_CHILD_BUTTON,
		NULL,
		_("Queue"),
		_("Create queues of saved fin traces, run queued matches in batch mode, and review saved match results."), NULL,
		tmp_toolbar_icon, NULL, NULL);
	gtk_button_set_relief(GTK_BUTTON(mainButtonMatchingQueue), GTK_RELIEF_NONE);
	gtk_widget_show(mainButtonMatchingQueue);

	//***1.85 - if database load failed do not allow this menu option
	if (mDatabase->status() != Database::loaded)
		gtk_widget_set_sensitive(mainButtonMatchingQueue, FALSE);
	mQueueButton = mainButtonMatchingQueue;

	// create "Options" button (shortcut to Options Dialog)

	/*
	//***1.5 - eliminated this button
	tmp_toolbar_icon = create_pixmap_from_data(mainWindow, options_xpm);
	mainButtonOptions = gtk_toolbar_append_element (GTK_TOOLBAR (mToolBar),
	GTK_TOOLBAR_CHILD_BUTTON,
	NULL,
	_("Options"),
	_("A number of settings that change the way DARWIN operates."), NULL,
	tmp_toolbar_icon, NULL, NULL);
	gtk_button_set_relief (GTK_BUTTON (mainButtonOptions), GTK_RELIEF_NONE);
	gtk_widget_show (mainButtonOptions);
	*/

	// create "Exit" button (Shortcut to Exit program)

	tmp_toolbar_icon = create_pixmap_from_data(mainWindow, exit_xpm);
	mainButtonExit = gtk_toolbar_append_element(GTK_TOOLBAR(mToolBar),
		GTK_TOOLBAR_CHILD_BUTTON,
		NULL,
		_("Exit"),
		_("Quit DARWIN."), NULL,
		tmp_toolbar_icon, NULL, NULL);
	gtk_button_set_relief(GTK_BUTTON(mainButtonExit), GTK_RELIEF_NONE);
	gtk_widget_show(mainButtonExit);

	/*
	//***1.85 - new box for tools above Clist and Image
	GtkWidget *tempHbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start (GTK_BOX (mainVBox), tempHbox, FALSE, FALSE, 0);
	gtk_widget_show(tempHbox);

	//***1.95 - new radio buttons to select view PRIMARY images only or view ALL
	GtkWidget *button = gtk_radio_button_new_with_label(
	NULL,
	_("Show ONLY Primary Images"));
	gtk_box_pack_start (GTK_BOX (tempHbox), button, FALSE, FALSE, 0);
	gtk_widget_show(button);
	button = gtk_radio_button_new_with_label_from_widget(
	GTK_RADIO_BUTTON(button),
	_("Show ALL Images"));
	gtk_box_pack_start (GTK_BOX (tempHbox), button, FALSE, FALSE, 0);
	gtk_widget_show(button);
	*/
	/*
	//***1.85 - new FindDolphin by ID tool
	tempHbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start (GTK_BOX (mainVBox), tempHbox, FALSE, FALSE, 0);
	gtk_widget_show(tempHbox);

	GtkWidget *findLabel = gtk_label_new("Find Dolphin by ID:");
	gtk_box_pack_start (GTK_BOX (tempHbox), findLabel, FALSE, FALSE, 0);
	gtk_widget_show(findLabel);

	GtkWidget *findEntry = gtk_entry_new();
	gtk_box_pack_start (GTK_BOX (tempHbox), findEntry, FALSE, FALSE, 0);
	gtk_widget_show(findEntry);

	mSearchID = findEntry;

	GtkWidget *findNow = gtk_button_new_with_label("Goto");
	gtk_box_pack_start (GTK_BOX (tempHbox), findNow, FALSE, FALSE, 0);
	gtk_widget_show(findNow);
	*/

	// set up rest of main window

	mainHPaned = gtk_hpaned_new();
	gtk_widget_show(mainHPaned);
	gtk_box_pack_start(GTK_BOX(mainVBox), mainHPaned, TRUE, TRUE, 0);

	leftFrame = gtk_frame_new(NULL);
	gtk_widget_show(leftFrame);
	gtk_frame_set_shadow_type(GTK_FRAME(leftFrame), GTK_SHADOW_IN);
	gtk_paned_pack1(GTK_PANED(mainHPaned), leftFrame, TRUE, TRUE);

	//***1.96 - new box for view list options
	mainLeftVBox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(mainLeftVBox);
	gtk_container_add(GTK_CONTAINER(leftFrame), mainLeftVBox);
	gtk_container_set_border_width(GTK_CONTAINER(mainLeftVBox), 4);

	//***1.96 - new radio buttons to select view PRIMARY images only or view ALL
	GtkWidget *tempHbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(mainLeftVBox), tempHbox, FALSE, FALSE, 0);
	//gtk_widget_show(tempHbox); //***1.96a - do NOT show/allow this option for now

	GtkWidget *showLabel = gtk_label_new(_("Showing: "));
	gtk_box_pack_start(GTK_BOX(tempHbox), showLabel, FALSE, FALSE, 0);
	gtk_widget_show(showLabel);

	GtkWidget *button = gtk_radio_button_new_with_label(
		NULL,
		_("ONLY Primary Images"));
	gtk_box_pack_start(GTK_BOX(tempHbox), button, FALSE, FALSE, 0);
	gtk_widget_show(button);

	button = gtk_radio_button_new_with_label_from_widget(
		GTK_RADIO_BUTTON(button),
		_("ALL Images"));
	gtk_box_pack_start(GTK_BOX(tempHbox), button, FALSE, FALSE, 0);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
	gtk_widget_show(button);
	//***1.96 - end 

	mainScrolledWindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(mainScrolledWindow);
	gtk_box_pack_start(GTK_BOX(mainLeftVBox), mainScrolledWindow, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(mainScrolledWindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

	// Configure GtkTreeView.  Mod by KLY.
	// http://maemo.org/api_refs/5.0/5.0-final/gtk/TreeWidget.html

	mScrollable = mainScrolledWindow;

	/* Create a model.  */
	mStore = gtk_tree_store_new(N_COLUMNS,
		GDK_TYPE_PIXBUF, /* PIXBUFF_COLUMN */
		G_TYPE_STRING, /* USER_SUPPLIED_ID_COLUMN*/
		G_TYPE_STRING, /* NAME_COLUMN */
		G_TYPE_STRING, /* DAMAGE_COLUMN*/
		G_TYPE_STRING, /* DATE_COLUMN */
		G_TYPE_STRING, /* LOCATION_COLUMN */
		G_TYPE_INT /* INDV_ID_COLUMN*/
		);


	/* Create a view */
	mView = gtk_tree_view_new_with_model(GTK_TREE_MODEL(mStore));

	/* Create a column, associating the "pixbuf" attribute of the
	* cell_renderer to the first column of the model	*/
	/* https://mail.gnome.org/archives/gtk-app-devel-list/2003-June/msg00026.html */
	mRenderer = gtk_cell_renderer_pixbuf_new();
	mPixBufColumn = gtk_tree_view_column_new_with_attributes("", mRenderer, "pixbuf", PIXBUFF_COLUMN, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mView), mPixBufColumn);

	/* Create a column, associating the "text" attribute of the
	* cell_renderer to this column of the model	*/
	mRenderer = gtk_cell_renderer_text_new();
	mUserSuppliedIDColumn = gtk_tree_view_column_new_with_attributes("ID", mRenderer, "text", USER_SUPPLIED_ID_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mUserSuppliedIDColumn, USER_SUPPLIED_ID_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mView), mUserSuppliedIDColumn);

	g_signal_connect(G_OBJECT(mUserSuppliedIDColumn), "clicked",
		GTK_SIGNAL_FUNC(on_mainwindow_header_click),
		(void *) this);

	mRenderer = gtk_cell_renderer_text_new();
	mNameColumn = gtk_tree_view_column_new_with_attributes("Name", mRenderer, "text", NAME_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mNameColumn, NAME_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mView), mNameColumn);

	g_signal_connect(G_OBJECT(mNameColumn), "clicked",
		GTK_SIGNAL_FUNC(on_mainwindow_header_click),
		(void *) this);

	mRenderer = gtk_cell_renderer_text_new();
	mDamageColumn = gtk_tree_view_column_new_with_attributes("Damage", mRenderer, "text", DAMAGE_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mDamageColumn, DAMAGE_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mView), mDamageColumn);

	g_signal_connect(G_OBJECT(mDamageColumn), "clicked",
		GTK_SIGNAL_FUNC(on_mainwindow_header_click),
		(void *) this);

	mRenderer = gtk_cell_renderer_text_new();
	mDateColumn = gtk_tree_view_column_new_with_attributes("Date", mRenderer, "text", DATE_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mDateColumn, DATE_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mView), mDateColumn);

	g_signal_connect(G_OBJECT(mDateColumn), "clicked",
		GTK_SIGNAL_FUNC(on_mainwindow_header_click),
		(void *) this);

	mRenderer = gtk_cell_renderer_text_new();
	mLocationColumn = gtk_tree_view_column_new_with_attributes("Location", mRenderer, "text", LOCATION_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mLocationColumn, LOCATION_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mView), mLocationColumn);

	g_signal_connect(G_OBJECT(mLocationColumn), "clicked",
		GTK_SIGNAL_FUNC(on_mainwindow_header_click),
		(void *) this);

	mRenderer = gtk_cell_renderer_text_new();
	mIndvIDColumn = gtk_tree_view_column_new_with_attributes("INDV ID", mRenderer, "text", INDV_ID_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mIndvIDColumn, INDV_ID_COLUMN);
	//gtk_tree_view_column_set_visible(mColumn, FALSE);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mView), mIndvIDColumn);

	/* Initialize our selection so that the g_signal_connect is created successfully. */
	mSelection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mView));

	/* The view now holds a reference.  We can get rid of our own reference */
	g_object_unref(mStore);

	// Allow the column title buttons to be clicked.
	gtk_tree_view_set_headers_clickable(GTK_TREE_VIEW(mView), true);
	// Sets the visibility state of the headers.
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(mView), true);

	gtk_container_add(GTK_CONTAINER(mainScrolledWindow), mView);
	gtk_widget_show(mView);

	//***1.85 - new FindDolphin by ID tool
	tempHbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(mainLeftVBox), tempHbox, FALSE, FALSE, 5);
	gtk_widget_show(tempHbox);

	GtkWidget *findLabel = gtk_label_new("To Find Dolphin by ID, click ID column and type ID");
	gtk_box_pack_start(GTK_BOX(tempHbox), findLabel, FALSE, FALSE, 0);
	gtk_widget_show(findLabel);

	/* Disable use of find entry box and use built-in search function.  Mod by KLY
	GtkWidget *findEntry = gtk_entry_new();
	gtk_box_pack_start (GTK_BOX (tempHbox), findEntry, FALSE, FALSE, 0);
	gtk_widget_show(findEntry);

	mSearchID = findEntry;

	GtkWidget *findNow = gtk_button_new_with_label("Goto");
	gtk_box_pack_start (GTK_BOX (tempHbox), findNow, FALSE, FALSE, 0);
	gtk_widget_show(findNow);
	*/

	// create button box with "Previous", "Next" and "Modify Database" buttons

	hbuttonbox1 = gtk_hbutton_box_new();
	gtk_widget_show(hbuttonbox1);
	gtk_box_pack_start(GTK_BOX(mainLeftVBox), hbuttonbox1, FALSE, TRUE, 5);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(hbuttonbox1), GTK_BUTTONBOX_SPREAD);

	// create Previous button (accelerator ALT-P)

	mButtonPrev = gtk_button_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, previous_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_Previous"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mButtonPrev);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, TRUE, TRUE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(mButtonPrev), tmpBox);

	gtk_widget_show(mButtonPrev);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), mButtonPrev);
	GTK_WIDGET_SET_FLAGS(mButtonPrev, GTK_CAN_DEFAULT);
	gtk_tooltips_set_tip(tooltips, mButtonPrev,
		_("Cycle to the previous fin in the database."), NULL);

	// create Next button (mnemonic ALT-N)

	mButtonNext = gtk_button_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, next_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_Next"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mButtonNext);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, TRUE, TRUE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(mButtonNext), tmpBox);

	gtk_widget_show(mButtonNext);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), mButtonNext);
	GTK_WIDGET_SET_FLAGS(mButtonNext, GTK_CAN_DEFAULT);
	gtk_tooltips_set_tip(tooltips, mButtonNext, _("Cycle to the next fin in the database."), NULL);

	// create Modify Database button (mnemonic ALT-M)

	//***002DB >
	mButtonModify = gtk_button_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, add_database_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_Modify"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mButtonModify);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, TRUE, TRUE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(mButtonModify), tmpBox);

	gtk_widget_show(mButtonModify);
	//***1.99 - this button goes with the data in the new notebook page below
	//gtk_container_add (GTK_CONTAINER (hbuttonbox1), mButtonModify);
	GTK_WIDGET_SET_FLAGS(mButtonModify, GTK_CAN_DEFAULT);
	gtk_tooltips_set_tip(tooltips, mButtonModify, _("Modify selected entry in the database."), NULL);
	//***002DB <


	mainInfoTable = gtk_table_new(7, 2, FALSE);
	gtk_widget_show(mainInfoTable);
	//***1.99 - the data table now goes inside a notebook page in the tabbed
	// view code below
	//gtk_box_pack_start (GTK_BOX (mainLeftVBox), mainInfoTable, FALSE, TRUE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(mainInfoTable), 3);
	gtk_table_set_row_spacings(GTK_TABLE(mainInfoTable), 4);
	gtk_table_set_col_spacings(GTK_TABLE(mainInfoTable), 5);

	mEntryName = gtk_entry_new();
	gtk_widget_show(mEntryName);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mEntryName, 1, 2, 1, 2,
		(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions)(0), 0, 0);
	gtk_entry_set_editable(GTK_ENTRY(mEntryName), FALSE);

	mEntryDate = gtk_entry_new();
	gtk_widget_show(mEntryDate);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mEntryDate, 1, 2, 2, 3,
		(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions)(0), 0, 0);
	gtk_entry_set_editable(GTK_ENTRY(mEntryDate), FALSE);

	mEntryRoll = gtk_entry_new();
	gtk_widget_show(mEntryRoll);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mEntryRoll, 1, 2, 3, 4,
		(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions)(0), 0, 0);
	gtk_entry_set_editable(GTK_ENTRY(mEntryRoll), FALSE);

	mEntryLocation = gtk_entry_new();
	gtk_widget_show(mEntryLocation);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mEntryLocation, 1, 2, 4, 5,
		(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions)(0), 0, 0);
	gtk_entry_set_editable(GTK_ENTRY(mEntryLocation), FALSE);

	mEntryDescription = gtk_entry_new();
	gtk_widget_show(mEntryDescription);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mEntryDescription, 1, 2, 6, 7,
		(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions)(0), 0, 0);
	gtk_entry_set_editable(GTK_ENTRY(mEntryDescription), FALSE);

	mainLabelID = gtk_label_new(_("ID Code"));
	gtk_widget_show(mainLabelID);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mainLabelID, 0, 1, 0, 1,
		(GtkAttachOptions)(0),
		(GtkAttachOptions)(0), 0, 0);

	mainLabelName = gtk_label_new(_("Name"));
	gtk_widget_show(mainLabelName);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mainLabelName, 0, 1, 1, 2,
		(GtkAttachOptions)(0),
		(GtkAttachOptions)(0), 0, 0);

	mainLabelDate = gtk_label_new(_("Date of Sighting"));
	gtk_widget_show(mainLabelDate);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mainLabelDate, 0, 1, 2, 3,
		(GtkAttachOptions)(0),
		(GtkAttachOptions)(0), 0, 0);

	//mainLabelRoll = gtk_label_new (_("Roll and Frame"));
	mainLabelRoll = gtk_label_new(_("Roll/Frame or Lat/Long")); //***2.22 - now Lat/Long
	gtk_widget_show(mainLabelRoll);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mainLabelRoll, 0, 1, 3, 4,
		(GtkAttachOptions)(0),
		(GtkAttachOptions)(0), 0, 0);

	mainLabelLocation = gtk_label_new(_("Location Code"));
	gtk_widget_show(mainLabelLocation);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mainLabelLocation, 0, 1, 4, 5,
		(GtkAttachOptions)(0),
		(GtkAttachOptions)(0), 0, 0);

	mainLabelDamage = gtk_label_new(_("Damage Category"));
	gtk_widget_show(mainLabelDamage);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mainLabelDamage, 0, 1, 5, 6,
		(GtkAttachOptions)(0),
		(GtkAttachOptions)(0), 0, 0);

	mainLabelDescription = gtk_label_new(_("Short Description"));
	gtk_widget_show(mainLabelDescription);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mainLabelDescription, 0, 1, 6, 7,
		(GtkAttachOptions)(0),
		(GtkAttachOptions)(0), 0, 0);

	mEntryID = gtk_entry_new();
	gtk_widget_show(mEntryID);
	gtk_table_attach(GTK_TABLE(mainInfoTable), mEntryID, 1, 2, 0, 1,
		(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
		(GtkAttachOptions)(0), 0, 0);
	gtk_entry_set_editable(GTK_ENTRY(mEntryID), FALSE);

	//***1.99 - the right frame will now contain a TABBED area for the
	// modified image, original image, data fields, outline, ...

	rightFrame = gtk_frame_new(NULL);
	gtk_widget_show(rightFrame);
	gtk_frame_set_shadow_type(GTK_FRAME(rightFrame), GTK_SHADOW_IN);
	gtk_paned_pack2(GTK_PANED(mainHPaned), rightFrame, TRUE, TRUE);

	mainRightVBox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(mainRightVBox);
	gtk_container_add(GTK_CONTAINER(rightFrame), mainRightVBox);

	//*** - here is the NEW tabbed notebook

	GtkWidget *notebook = gtk_notebook_new();

	gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
	gtk_box_pack_start(GTK_BOX(mainRightVBox), notebook, TRUE, TRUE, 0);
	gtk_widget_show(notebook);

	GtkWidget *tempLabel; // for tab text

	// now create a framed view for the MODIFIED IMAGE and put it in the notebook
	// as page number 1

	mFrameMod = gtk_frame_new(_("(ID Code)"));
	gtk_container_set_border_width(GTK_CONTAINER(mFrameMod), 3);
	gtk_frame_set_label_align(GTK_FRAME(mFrameMod), 1.0, 0.0);
	gtk_widget_show(mFrameMod);
	tempLabel = gtk_label_new(_("Modified Image"));
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), mFrameMod, tempLabel);
	gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), 0); // this is default page

	mainEventBoxImage = gtk_event_box_new();
	gtk_widget_show(mainEventBoxImage);
	gtk_container_add(GTK_CONTAINER(mFrameMod), mainEventBoxImage);

	mDrawingAreaImage = gtk_drawing_area_new();
	gtk_widget_show(mDrawingAreaImage);
	gtk_container_add(GTK_CONTAINER(mainEventBoxImage), mDrawingAreaImage);

	gtk_drawing_area_size(GTK_DRAWING_AREA(mDrawingAreaImage), IMAGE_WIDTH, IMAGE_HEIGHT);

	// now create a framed view for the ORIGINAL IMAGE and put it in the notebook
	// as page number 2

	mFrameOrig = gtk_frame_new(_("(ID Code)"));
	gtk_container_set_border_width(GTK_CONTAINER(mFrameOrig), 3);
	gtk_frame_set_label_align(GTK_FRAME(mFrameOrig), 1.0, 0.0);
	gtk_widget_show(mFrameOrig);
	tempLabel = gtk_label_new(_("Original Image"));
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), mFrameOrig, tempLabel);
	gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), 0); // this is default page

	GtkWidget *mainEventBoxOrigImage = gtk_event_box_new();
	gtk_widget_show(mainEventBoxOrigImage);
	gtk_container_add(GTK_CONTAINER(mFrameOrig), mainEventBoxOrigImage);

	mDrawingAreaOrigImage = gtk_drawing_area_new();
	gtk_widget_show(mDrawingAreaOrigImage);
	gtk_container_add(GTK_CONTAINER(mainEventBoxOrigImage), mDrawingAreaOrigImage);

	gtk_drawing_area_size(GTK_DRAWING_AREA(mDrawingAreaOrigImage), IMAGE_WIDTH, IMAGE_HEIGHT);

	// now create a framed view for the fin OUTLINE and put it in the notebook
	// as page number 3

	mainFrameOutline = gtk_frame_new(_("Fin Outline"));
	gtk_container_set_border_width(GTK_CONTAINER(mainFrameOutline), 3);
	gtk_frame_set_label_align(GTK_FRAME(mainFrameOutline), 1.0, 0.0);
	gtk_widget_show(mainFrameOutline);
	tempLabel = gtk_label_new(_("Fin Outline"));
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), mainFrameOutline, tempLabel);

	mainEventBoxOutline = gtk_event_box_new();
	gtk_widget_show(mainEventBoxOutline);
	gtk_container_add(GTK_CONTAINER(mainFrameOutline), mainEventBoxOutline);

	mDrawingAreaOutline = gtk_drawing_area_new();
	gtk_widget_show(mDrawingAreaOutline);
	gtk_container_add(GTK_CONTAINER(mainEventBoxOutline), mDrawingAreaOutline);

	gtk_drawing_area_size(GTK_DRAWING_AREA(mDrawingAreaOutline), IMAGE_WIDTH / 4, IMAGE_HEIGHT / 4);

	// now create a framed view for the fin DATA and put it in the notebook
	// as page number 4
	GtkWidget *dataFrame = gtk_frame_new(_("Fin Data"));
	gtk_container_set_border_width(GTK_CONTAINER(dataFrame), 3);
	gtk_frame_set_label_align(GTK_FRAME(dataFrame), 1.0, 0.0);
	gtk_widget_show(dataFrame);
	tempLabel = gtk_label_new(_("Fin Data"));
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), dataFrame, tempLabel);
	// connect data table to this new frame
	gtk_container_add(GTK_CONTAINER(dataFrame), mainInfoTable);

	GtkWidget *hbuttonbox2 = gtk_hbutton_box_new();
	gtk_widget_show(hbuttonbox2);
	gtk_container_add(GTK_CONTAINER(mainRightVBox), hbuttonbox2);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(hbuttonbox2), GTK_BUTTONBOX_SPREAD);

	gtk_container_add(GTK_CONTAINER(hbuttonbox2), mButtonModify);


	// FINALLY, at bottom of main window is a status bar

	mStatusBar = gtk_statusbar_new();
	gtk_widget_show(mStatusBar);

	mContextID = gtk_statusbar_get_context_id(GTK_STATUSBAR(mStatusBar), "mainWin");

	gtk_box_pack_start(GTK_BOX(mainVBox), mStatusBar, FALSE, FALSE, 0);

	g_signal_connect(G_OBJECT(mainWindow), "delete_event",
		GTK_SIGNAL_FUNC(on_mainWindow_delete_event),
		(void *) this);
	g_signal_connect(G_OBJECT(open), "activate",
		GTK_SIGNAL_FUNC(on_open_activate),
		(void *) this);
	//***1.4 - new callback for opening previously saved fin tracing
	g_signal_connect(G_OBJECT(openTFin), "activate",
		GTK_SIGNAL_FUNC(on_open_fin_trace_activate),
		(void *) this);
	//***1.85 - new callback for opening a database
	g_signal_connect(G_OBJECT(opendb), "activate",
		GTK_SIGNAL_FUNC(on_open_database_activate),
		(void *) this);
	//***1.85 - new callback for creating a database
	g_signal_connect(G_OBJECT(newdb), "activate",
		GTK_SIGNAL_FUNC(on_new_database_activate),
		(void *) this);
	g_signal_connect(G_OBJECT(matching_queue), "activate",
		GTK_SIGNAL_FUNC(on_matching_queue_activate),
		(void *) this);
	//***1.85 - new callbacks for backing up or exporting a database
	g_signal_connect(G_OBJECT(backup), "activate",
		GTK_SIGNAL_FUNC(on_backup_database_activate),
		(void *) this);
	g_signal_connect(G_OBJECT(mExportDBMenuItem), "activate",
		GTK_SIGNAL_FUNC(on_export_database_activate),
		(void *) this);
	//***1.99 - callback for exporting a Fin
	g_signal_connect(G_OBJECT(mExportFinzMenuItem), "activate",
		GTK_SIGNAL_FUNC(on_export_finz_activate),
		(void *) this);
	//***2.02 - callback for exporting Full-Size Modified Images
	g_signal_connect(G_OBJECT(mExportFullSzImgsMenuItem), "activate",
		GTK_SIGNAL_FUNC(on_export_fullSzImgs_activate),
		(void *) this);

	//***1.99 - callback for importing Fin
	g_signal_connect(G_OBJECT(mImportFinzMenuItem), "activate",
		GTK_SIGNAL_FUNC(on_import_finz_activate),
		(void *) this);


	//***1.85 - new callbacks for restoring or importing a database
	g_signal_connect(G_OBJECT(restore), "activate",
		GTK_SIGNAL_FUNC(on_restore_database_activate),
		(void *) this);
	g_signal_connect(G_OBJECT(importDB), "activate",
		GTK_SIGNAL_FUNC(on_import_database_activate),
		(void *) this);

	g_signal_connect(G_OBJECT(exit), "activate",
		GTK_SIGNAL_FUNC(on_exit_activate),
		(void *) this);

	//***1.4 - three new Catalog Scheme related callbacks
	g_signal_connect(G_OBJECT(catalog_new), "activate",
		GTK_SIGNAL_FUNC(on_catalog_new_activate),
		(void *) this);
	g_signal_connect(G_OBJECT(catalog_view), "activate",
		GTK_SIGNAL_FUNC(on_catalog_view_activate),
		(void *) this);
	g_signal_connect(G_OBJECT(catalog_select), "activate",
		GTK_SIGNAL_FUNC(on_catalog_select_activate),
		(void *) this);

	//***1.9 - callback for data export in spreadsheet format
	g_signal_connect(G_OBJECT(exportData), "activate",
		GTK_SIGNAL_FUNC(on_exportData_select_activate),
		(void *) this);

	g_signal_connect(G_OBJECT(options), "activate",
		GTK_SIGNAL_FUNC(on_options_activate),
		(void *) this);
	g_signal_connect(G_OBJECT(about), "activate",
		GTK_SIGNAL_FUNC(on_about_activate),
		(void *) this);
	g_signal_connect(G_OBJECT(docs), "activate",
		GTK_SIGNAL_FUNC(on_docs_activate),
		(void *) this);
	g_signal_connect(G_OBJECT(mainButtonOpen), "clicked",
		GTK_SIGNAL_FUNC(on_mainButtonOpen_clicked),
		(void *) this);
	//***1.85 -new callback
	/* Removed since we're using built-in Store search function .
	g_signal_connect (G_OBJECT (findNow), "clicked",
	GTK_SIGNAL_FUNC (on_mainButtonFindNow_clicked),
	(void *) this);
	*/
	//***1.5 - new callback
	g_signal_connect(G_OBJECT(mainButtonOpenTrace), "clicked",
		GTK_SIGNAL_FUNC(on_mainButtonOpenTrace_clicked),
		(void *) this);
	g_signal_connect(G_OBJECT(mainButtonMatchingQueue), "clicked",
		GTK_SIGNAL_FUNC(on_mainButtonMatchingQueue_clicked),
		(void *) this);
	//g_signal_connect (G_OBJECT (mainButtonOptions), "clicked",
	//                    GTK_SIGNAL_FUNC (on_mainButtonOptions_clicked),
	//                    (void *) this);
	g_signal_connect(G_OBJECT(mainButtonExit), "clicked",
		GTK_SIGNAL_FUNC(on_mainButtonExit_clicked),
		(void *) this);
	/* Mod by KLY. */
	g_signal_connect(G_OBJECT(mSelection), "changed",
		GTK_SIGNAL_FUNC(on_mainTreeView_select_row),
		(void *) this);
	g_signal_connect(G_OBJECT(mButtonPrev), "clicked",
		GTK_SIGNAL_FUNC(on_mainButtonPrev_clicked),
		(void *) this);
	g_signal_connect(G_OBJECT(mButtonNext), "clicked",
		GTK_SIGNAL_FUNC(on_mainButtonNext_clicked),
		(void *) this);
	g_signal_connect(G_OBJECT(mButtonModify), "clicked",	//***002DB
		GTK_SIGNAL_FUNC(on_mainButtonModify_clicked),
		(void *) this);
	g_signal_connect(G_OBJECT(mainEventBoxImage), "button_press_event",
		GTK_SIGNAL_FUNC(on_mainEventBoxImage_button_press_event),
		(void *) this);
	//***1.99 - new callback for popup viewer containing Original Image
	g_signal_connect(G_OBJECT(mainEventBoxOrigImage), "button_press_event",
		GTK_SIGNAL_FUNC(on_mainEventBoxOrigImage_button_press_event),
		(void *) this);
	g_signal_connect(G_OBJECT(mDrawingAreaImage), "expose_event",
		GTK_SIGNAL_FUNC(on_mainDrawingAreaImage_expose_event),
		(void *) this);
	//***1.99 - new callback for exposure of Original image
	g_signal_connect(G_OBJECT(mDrawingAreaOrigImage), "expose_event",
		GTK_SIGNAL_FUNC(on_mainDrawingAreaOrigImage_expose_event),
		(void *) this);
	g_signal_connect(G_OBJECT(mainEventBoxOutline), "button_press_event",
		GTK_SIGNAL_FUNC(on_mainEventBoxOutline_button_press_event),
		(void *) this);
	g_signal_connect(G_OBJECT(mDrawingAreaOutline), "expose_event",
		GTK_SIGNAL_FUNC(on_mainDrawingAreaOutline_expose_event),
		(void *) this);

	g_signal_connect(G_OBJECT(mDrawingAreaImage), "configure_event",
		GTK_SIGNAL_FUNC(on_mainDrawingAreaImage_configure_event),
		(void *) this);
	//***1.99 - new callback for showing original image
	g_signal_connect(G_OBJECT(mDrawingAreaOrigImage), "configure_event",
		GTK_SIGNAL_FUNC(on_mainDrawingAreaOrigImage_configure_event),
		(void *) this);

	g_signal_connect(G_OBJECT(mDrawingAreaOutline), "configure_event",
		GTK_SIGNAL_FUNC(on_mainDrawingAreaOutline_configure_event),
		(void *) this);

	gtk_widget_grab_default(mButtonNext);
	g_object_set_data(G_OBJECT(mainWindow), "tooltips", tooltips);

	gtk_window_add_accel_group(GTK_WINDOW(mainWindow), accel_group);

	return mainWindow;
}

//*******************************************************************
gboolean on_mainWindow_delete_event(
	GtkWidget *widget,
	GdkEvent *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	delete mainWin;

	return TRUE;
}

//*******************************************************************
void on_open_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	if (getNumOpenFileChooserDialogReferences() < 1) {
		OpenFileChooserDialog *dlg = new OpenFileChooserDialog(
			mainWin->mDatabase,
			mainWin,
			mainWin->mOptions,
			OpenFileChooserDialog::openFinImage);
		dlg->run_and_respond();
	}
}

//*******************************************************************
//***1.4 - new
//
void on_open_fin_trace_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	if (getNumOpenFileChooserDialogReferences() < 1) {
		OpenFileChooserDialog *dlg = new OpenFileChooserDialog(
			mainWin->mDatabase,
			mainWin,
			mainWin->mOptions,
			OpenFileChooserDialog::openFinTrace);
		dlg->run_and_respond();
	}
}
//*******************************************************************
//***1.85 - new
//
void on_open_database_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	if (getNumOpenFileChooserDialogReferences() < 1) {
		OpenFileChooserDialog *dlg = new OpenFileChooserDialog(
			mainWin->mDatabase,
			mainWin,
			mainWin->mOptions,
			OpenFileChooserDialog::openDatabase);
		dlg->run_and_respond();
	}

	// reset the database filename for the window
	// set sensitivity of menu items and buttons depending on success
	// and create an emergency backup of DB file, as long as it was successfully loaded

	mainWin->resetTitleButtonsAndBackupOnDBLoad();
}

//*******************************************************************
//***1.85 - new
//
void on_new_database_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;
	
	CreateDatabaseDialog *dlg = new CreateDatabaseDialog(
		mainWin,
		mainWin->mOptions,
		""); // no archive being IMPORTED

	dlg->show();
}

//*******************************************************************
//***1.85 - new
//
void on_restore_database_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	// go find backup or previously exported archive
	// NOTE: difference between a backup and an export is whether or not the
	// additional folders (tracedFins, ...) are included

	OpenFileChooserDialog
		*open = new OpenFileChooserDialog(
		mainWin->mDatabase,
		mainWin,
		mainWin->mOptions,
		OpenFileChooserDialog::restoreDatabase);

	open->run_and_respond();

	// ALL OF THE FOLLOWING ...
	// is done in OpenFileChooserDialog::on_fileChooserButtonOK_clicked()
	//
	// determine which survey area it goes into
	// NOTE: this is NOT necessary for restore but is for import
	// build file structure, if needed
	// NOTE: again, not necessary for restore, but is for import
	// extract files into catalog folder
	// default action is to "NOT overwrite" existing files and to "NOT duplicate"
	// existing files 

	//***1.982 - we assume new database was opened after restore so ...
	// reset the database filename for the window
	// set sensitivity of menu items and buttons depending on success
	// and create an emergency backup of DB file, as long as it was successfully loaded
	// NOTE: the "keep_above" calls are to keep MainWindow on top of Console Window
	// which for some reason it wants to hide behind here

	gtk_window_set_keep_above(GTK_WINDOW(mainWin->getWindow()), TRUE);
	mainWin->resetTitleButtonsAndBackupOnDBLoad();
	gtk_window_set_keep_above(GTK_WINDOW(mainWin->getWindow()), FALSE);
}

//*******************************************************************
//***1.85 - new
//
void on_import_database_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	// go find backup or previously exported archive
	// NOTE: difference between a backup and an export is whether or not the
	// additional folders (tracedFins, ...) are included

	mainWin->mImportFromFilename = "";

	OpenFileChooserDialog
		*open = new OpenFileChooserDialog(
		mainWin->mDatabase,
		mainWin,
		mainWin->mOptions,
		OpenFileChooserDialog::importDatabase);

	open->run_and_respond();

	// if no filename was acqured, then import was cancelled
	if ("" == mainWin->mImportFromFilename)
		return;

	// otherwise, continue with import process
	CreateDatabaseDialog *dlg = new CreateDatabaseDialog(
		mainWin,
		mainWin->mOptions,
		mainWin->mImportFromFilename); // name of archive being IMPORTED

	dlg->show();

	// ALL OF THE FOLLOWING ...
	// is done in OpenFileChooserDialog::on_fileChooserButtonOK_clicked()
	//
	// determine which survey area it goes into
	// NOTE: this is NOT necessary for restore but is for import
	// build file structure, if needed
	// NOTE: again, not necessary for restore, but is for import
	// extract files into catalog folder
	// default action is to "NOT overwrite" existing files and to "NOT duplicate"
	// existing files 

	//***1.982 - we assume new database was opened after restore so ...
	// reset the database filename for the window
	// set sensitivity of menu items and buttons depending on success
	// and create an emergency backup of DB file, as long as it was successfully loaded
	// NOTE: the "keep_above" calls are to keep MainWindow on top of Console Window
	// which for some reason it wants to hide behind here

	gtk_window_set_keep_above(GTK_WINDOW(mainWin->getWindow()), TRUE);
	mainWin->resetTitleButtonsAndBackupOnDBLoad();
	gtk_window_set_keep_above(GTK_WINDOW(mainWin->getWindow()), FALSE);

}

//*******************************************************************
//***1.85 - new
//
void on_backup_database_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	// let user know what is happening and then close iconify main window so user
	// can see backup progress in console window
	//***1.93 - begin new code segment
	GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(mainWin->getWindow()),
		GTK_DIALOG_DESTROY_WITH_PARENT,
		GTK_MESSAGE_INFO,
		GTK_BUTTONS_OK,
		"BACKUP starting ... \nDetails will appear in CONSOLE window.");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	gtk_window_iconify(GTK_WINDOW(mainWin->getWindow())); //***1.93
	//***1.93 - end new code segment

	if (!backupCatalog(mainWin->mDatabase))
	{
		//***2.22 - added mainWin->mWindow
		//ErrorDialog *err = new ErrorDialog(mainWin->mWindow,"Database backup failed.");
		//err->show();
		//***2.22 - replacing own ErrorDialog with GtkMessageDialogs
		GtkWidget *errd = gtk_message_dialog_new(GTK_WINDOW(mainWin->mWindow),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_CLOSE,
			"Database backup failed.");
		gtk_dialog_run(GTK_DIALOG(errd));
		gtk_widget_destroy(errd);
	}

	// must check here to ensure database reopened correctly
	if (!mainWin->mDatabase->isOpen())
	{
		//***2.22 - added mainWin->mWindow
		//ErrorDialog *err = new ErrorDialog(mainWin->mWindow,"Database failed to reopen");
		//err->show();
		//***2.22 - replacing own ErrorDialog with GtkMessageDialogs
		GtkWidget *errd = gtk_message_dialog_new(GTK_WINDOW(mainWin->mWindow),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_CLOSE,
			"Database failed to reopen.");
		gtk_dialog_run(GTK_DIALOG(errd));
		gtk_widget_destroy(errd);
	}

	// reopen main window in front of the console window
	gtk_window_set_keep_above(GTK_WINDOW(mainWin->getWindow()), TRUE); //***1.93
	gtk_window_deiconify(GTK_WINDOW(mainWin->getWindow())); //***1.93
	gtk_window_set_keep_above(GTK_WINDOW(mainWin->getWindow()), FALSE); //***1.93

}

//*******************************************************************
//***1.99 - new
//
void on_export_finz_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	//Save the currently selected fin for the moment (should show dialog allowing for multiple fin selection (or all fins))
	MainWindow *mainWin = (MainWindow *)userData;
	/*CatalogSupport*///saveFinz(mainWin->mSelectedFin,"test.finz");
	ExportFinzDialog *dlg = new ExportFinzDialog(
		mainWin->mDatabase,
		mainWin->mWindow,
		mainWin->mView,
		ExportFinzDialog::exportFinz); //2.02 - set operating mode
	dlg->show();
}

//*******************************************************************
//***2.02 - new
//
void on_export_fullSzImgs_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	// Save the modified image of the currently selected fin FULL_SIZED

	MainWindow *mainWin = (MainWindow *)userData;

	/*
	// get filename for modified image (including path)
	string imageSaveFileName = mainWin->mSelectedFin->mImageFilename;
	// strip extension and append "_fullSize.png"
	imageSaveFileName = imageSaveFileName.substr(0,imageSaveFileName.rfind("."));
	imageSaveFileName += "_fullSize.png";

	// save full-sized version in catalog folder alongside thumbnail-only version
	mainWin->mImageFullsize->save(imageSaveFileName);
	*/

	ExportFinzDialog *dlg = new ExportFinzDialog(

		mainWin->mDatabase,
		mainWin->mWindow,
		mainWin->mView,
		ExportFinzDialog::exportFullSizeModImages); //2.02 - set operating mode
	dlg->show();
}


//*******************************************************************
//***1.99 - new
//
void on_import_finz_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;
	if (NULL == mainWin)
		return;

	OpenFileChooserDialog *dlg = new OpenFileChooserDialog(
		mainWin->mDatabase,
		mainWin,
		mainWin->mOptions,
		OpenFileChooserDialog::directlyImportFinz);
	dlg->run_and_respond();


	mainWin->refreshDisplayFromDB();

}


//*******************************************************************
//***1.85 - new
//
void on_export_database_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	cout << "\nEXPORTING Database ...\n  " << mainWin->mOptions->mDatabaseFileName << endl;

	mainWin->mExportToFilename = "";

	// let user decide where to put the EXPORT - now uses SaveFileChooserDialog

	SaveFileChooserDialog
		*save = new SaveFileChooserDialog(
		mainWin->mDatabase,
		NULL, // no DatabaseFin to pass
		mainWin,
		NULL, // no TraceWindow involved
		mainWin->mOptions,
		mainWin->mWindow,
		SaveFileChooserDialog::exportDatabase);

	save->run_and_respond();

	// now mainWin->mExportToFilename is either "" or it contains the name
	// of a file into which the archive will be written

	if (mainWin->mExportToFilename == "")
	{
		cout << "EXPORT aborted - User cancel, or NO filename specified!" << endl;
		return;
	}

	if (!exportCatalogTo(mainWin->mDatabase, mainWin->mOptions, mainWin->mExportToFilename))
	{
		//***2.22 - added mainWin->mWindow
		//ErrorDialog *err = new ErrorDialog(mainWin->mWindow,"Catalog EXPORT failed.");
		//err->show();
		//***2.22 - replacing own ErrorDialog with GtkMessageDialogs
		GtkWidget *errd = gtk_message_dialog_new(GTK_WINDOW(mainWin->mWindow),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_CLOSE,
			"Catalog EXPORT failed.");
		gtk_dialog_run(GTK_DIALOG(errd));
		gtk_widget_destroy(errd);
	}

	if (!mainWin->mDatabase->isOpen())
	{
		//***2.22 - added mainWin->mWindow
		//ErrorDialog *err = new ErrorDialog(mainWin->mWindow,"Catalog failed to reopen");
		//err->show();
		//***2.22 - replacing own ErrorDialog with GtkMessageDialogs
		GtkWidget *errd = gtk_message_dialog_new(GTK_WINDOW(mainWin->mWindow),
			GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_CLOSE,
			"Catalog failed to reopen.");
		gtk_dialog_run(GTK_DIALOG(errd));
		gtk_widget_destroy(errd);
	}
}

//*******************************************************************
void on_matching_queue_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	//***1.0 -- swap commented / uncommented code below as needed
	//       for production / testing versions

	//***1.1 - let's get matching queue to work - JHS
	//showError("The Matching Queue option\nis not available in this\nsoftware version!"); //***054

	if (getNumMatchingQueueDialogReferences() < 1) {
		MatchingQueueDialog *dlg =
			new MatchingQueueDialog(mainWin, mainWin->mDatabase, mainWin->mOptions);
		dlg->show();
	}
}

//*******************************************************************
void on_exit_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	delete mainWin;
}

//*******************************************************************
void on_options_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	if (getNumOptionsDialogReferences() > 0)
		return;

	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	OptionsDialog *dlg = new OptionsDialog(mainWin, mainWin->mOptions);
	dlg->show();
}

//*******************************************************************
//
//***1.4 - new function
//
void on_catalog_new_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	if (getNumCatalogSchemeDialogReferences() > 0)
		return;

	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	CatalogSchemeDialog *dlg = new CatalogSchemeDialog(
		mainWin->getWindow(),
		mainWin->mOptions,
		CatalogSchemeDialog::defineNewScheme);
	dlg->show();
}

//*******************************************************************
//
//***1.4 - new function
//
void on_catalog_view_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	if (getNumCatalogSchemeDialogReferences() > 0)
		return;

	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	CatalogSchemeDialog *dlg = new CatalogSchemeDialog(
		mainWin->getWindow(),
		mainWin->mOptions,
		CatalogSchemeDialog::viewExistingSchemes);
	dlg->show();
}

//*******************************************************************
//
//***1.4 - new function
//
void on_catalog_select_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	if (getNumCatalogSchemeDialogReferences() > 0)
		return;

	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	CatalogSchemeDialog *dlg = new CatalogSchemeDialog(
		mainWin->getWindow(),
		mainWin->mOptions,
		CatalogSchemeDialog::setDefaultScheme);
	dlg->show();
}

//*******************************************************************
//***1.9 - new callback
//
void on_exportData_select_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	if (getNumDataExportDialogReferences() > 0)
		return;

	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	DataExportDialog *dlg = new DataExportDialog(
		mainWin->mDatabase,
		mainWin,
		mainWin->mOptions,
		DataExportDialog::saveToFile);

	dlg->run_and_respond();
}

//*******************************************************************
void on_mainwindow_header_click(GtkTreeViewColumn *treeviewcolumn, gpointer user_data) {
	/* Set the scroll position so that we can see the selected record. */
	MainWindow *mainWin = (MainWindow *)user_data;

	if (NULL == mainWin)
		return;

	GtkTreeIter iter;

	/* Get currently selected record. */
	gtk_tree_selection_get_selected(mainWin->mSelection, NULL, &iter);
	/* Unselect current iter. */
	gtk_tree_selection_unselect_iter(mainWin->mSelection, &iter);
	/* Reselect our record to initiate row selected events. */
	gtk_tree_selection_select_iter(mainWin->mSelection, &iter);
}

//*******************************************************************
void on_about_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	if (getNumAboutDialogReferences() < 1) {
		AboutDialog *about = new AboutDialog(mainWin->mWindow);
		about->show();
	}
}

//*******************************************************************
void on_docs_activate(
	GtkMenuItem *menuitem,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;


	//SAH 2008-07-18
	string cmd;
#ifdef WIN32
	cmd = "explorer.exe ";
	//system("explorer.exe %DARWINHOME%\\docs\\usersguide.htm&");
#else
	cmd = "open "; //***2.22 - for the Mac
	//system("open file:$DARWINHOME/docs/usersguide.htm&");
#endif
	cmd += "\"" + gOptions->mDarwinHome + PATH_SLASH + "docs" + PATH_SLASH + "usersguide.htm\" &";
	system(cmd.c_str());
}

//*******************************************************************
void on_mainButtonOpen_clicked(
	GtkButton *button,
	gpointer userData)
{
	on_open_activate(NULL, userData);
}

//*******************************************************************
void on_mainButtonOpenTrace_clicked(
	GtkButton *button,
	gpointer userData)
{
	on_open_fin_trace_activate(NULL, userData);
}

//*******************************************************************
void on_mainButtonMatchingQueue_clicked(
	GtkButton *button,
	gpointer userData)
{
	on_matching_queue_activate(NULL, userData);
}

//*******************************************************************
void on_mainButtonOptions_clicked(
	GtkButton *button,
	gpointer userData)
{
	on_options_activate(NULL, userData);
}

//*******************************************************************
void on_mainButtonExit_clicked(
	GtkButton *button,
	gpointer userData)
{
	on_exit_activate(NULL, userData);
}


//*******************************************************************
void on_mainTreeView_select_row(
	GtkTreeSelection *selection,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	/* Check to see if a record is selected, without moving the iteration. */
	if (!gtk_tree_selection_get_selected(selection, NULL, NULL))
		return;

	int selectedIndvID;
	
	if (NULL == mainWin->mSelectedFin) {
		selectedIndvID = 0;
	}
	else {
		selectedIndvID = mainWin->mSelectedFin->mDataPos;
		delete mainWin->mSelectedFin;
		mainWin->mSelectedFin = NULL;
	}

	GtkTreePath *current_path;
	GtkTreeIter iter;

	int indvID(0);

	try {

		/* Get currently selected record. */
		gtk_tree_selection_get_selected(selection, NULL, &iter);

		/* Get our data from the row. */
		gtk_tree_model_get(mainWin->mModel, &iter, mainWin->INDV_ID_COLUMN, &indvID, -1);
		if (selectedIndvID == indvID) {
			/* Nothing to due here.  Probably a false signal. */
			if (mainWin->mSelectedFin == NULL) {
				mainWin->mSelectedFin = mainWin->mDatabase->getItemAbsolute(indvID);
			}
			return;
		}
		else {
			selectedIndvID = indvID;
			mainWin->mSelectedFin = mainWin->mDatabase->getItemAbsolute(indvID);
		}

		if (NULL == mainWin->mSelectedFin)
			throw Error("Internal Error:\nProblem retrieving fin from database.");

		/* Get the current path from the iteration. */
		current_path = gtk_tree_model_get_path(mainWin->mModel, &iter);

		/* Determine if our Previous button is displayed. */
		if (!gtk_tree_path_prev(current_path))
			gtk_widget_set_sensitive(mainWin->mButtonPrev, FALSE);
		else
			gtk_widget_set_sensitive(mainWin->mButtonPrev, TRUE);

		/* Get currently selected record. */
		gtk_tree_selection_get_selected(selection, NULL, &iter);
		/* Get the current path from the iteration. */
		current_path = gtk_tree_model_get_path(mainWin->mModel, &iter);

		/* Determine if our next button is displayed. */
		gtk_tree_path_next(current_path);
		if (!gtk_tree_model_get_iter(mainWin->mModel, &iter, current_path))
			gtk_widget_set_sensitive(mainWin->mButtonNext, FALSE);
		else
			gtk_widget_set_sensitive(mainWin->mButtonNext, TRUE);

		/* Get currently selected record.  Reset mIter to where it was coming into this function. */
		gtk_tree_selection_get_selected(selection, NULL, &iter);

		/* Get the row position so we can adjust our scroll window so we can see this record. */
		gint *rowpos;
		rowpos = gtk_tree_path_get_indices(current_path);
		mainWin->mDBCurEntry = rowpos[0];

		gtk_tree_path_free(current_path);

		gchar
			*idcode,
			*name,
			*date,
			*roll,
			*location,
			*damage,
			*shortdescription;

		DatabaseFin<ColorImage> *t = mainWin->mSelectedFin;

		if (t->mIDCode == "NONE") {
			idcode = new gchar[strlen(NONE_SUBSTITUTE_STRING) + 1];
			strcpy(idcode, NONE_SUBSTITUTE_STRING);
			gtk_widget_set_sensitive(mainWin->mEntryID, FALSE);
		}
		else {
			idcode = new gchar[t->mIDCode.length() + 1];
			strcpy(idcode, t->mIDCode.c_str());
			gtk_widget_set_sensitive(mainWin->mEntryID, TRUE);
		}

		if (t->mName == "NONE") {
			name = new gchar[strlen(NONE_SUBSTITUTE_STRING) + 1];
			strcpy(name, NONE_SUBSTITUTE_STRING);
			gtk_widget_set_sensitive(mainWin->mEntryName, FALSE);
		}
		else {
			name = new gchar[t->mName.length() + 1];
			strcpy(name, t->mName.c_str());
			gtk_widget_set_sensitive(mainWin->mEntryName, TRUE);
		}

		if (t->mDateOfSighting == "NONE") {
			date = new gchar[strlen(NONE_SUBSTITUTE_STRING) + 1];
			strcpy(date, NONE_SUBSTITUTE_STRING);
			gtk_widget_set_sensitive(mainWin->mEntryDate, FALSE);
		}
		else {
			date = new gchar[t->mDateOfSighting.length() + 1];
			strcpy(date, t->mDateOfSighting.c_str());
			gtk_widget_set_sensitive(mainWin->mEntryDate, TRUE);
		}

		if (t->mRollAndFrame == "NONE") {
			roll = new gchar[strlen(NONE_SUBSTITUTE_STRING) + 1];
			strcpy(roll, NONE_SUBSTITUTE_STRING);
			gtk_widget_set_sensitive(mainWin->mEntryRoll, FALSE);
		}
		else {
			roll = new gchar[t->mRollAndFrame.length() + 1];
			strcpy(roll, t->mRollAndFrame.c_str());
			gtk_widget_set_sensitive(mainWin->mEntryRoll, TRUE);
		}

		if (t->mLocationCode == "NONE") {
			location = new gchar[strlen(NONE_SUBSTITUTE_STRING) + 1];
			strcpy(location, NONE_SUBSTITUTE_STRING);
			gtk_widget_set_sensitive(mainWin->mEntryLocation, FALSE);
		}
		else {
			location = new gchar[t->mLocationCode.length() + 1];
			strcpy(location, t->mLocationCode.c_str());
			gtk_widget_set_sensitive(mainWin->mEntryLocation, TRUE);
		}

		if (t->mShortDescription == "NONE") {
			shortdescription = new gchar[strlen(NONE_SUBSTITUTE_STRING) + 1];
			strcpy(shortdescription, NONE_SUBSTITUTE_STRING);
			gtk_widget_set_sensitive(mainWin->mEntryDescription, FALSE);
		}
		else {
			shortdescription = new gchar[t->mShortDescription.length() + 1];
			strcpy(shortdescription, t->mShortDescription.c_str());
			gtk_widget_set_sensitive(mainWin->mEntryDescription, TRUE);
		}
		//***1.65 - show or hide ID as setting indicates
		if (mainWin->mOptions->mHideIDs)
			gtk_entry_set_text(GTK_ENTRY(mainWin->mEntryID), "****");
		else
			gtk_entry_set_text(GTK_ENTRY(mainWin->mEntryID), idcode);
		gtk_entry_set_text(GTK_ENTRY(mainWin->mEntryName), name);
		gtk_entry_set_text(GTK_ENTRY(mainWin->mEntryDate), date);
		gtk_entry_set_text(GTK_ENTRY(mainWin->mEntryRoll), roll);
		gtk_entry_set_text(GTK_ENTRY(mainWin->mEntryLocation), location);
		gtk_entry_set_text(GTK_ENTRY(mainWin->mEntryDescription), shortdescription);

		gtk_frame_set_label(GTK_FRAME(mainWin->mFrameMod), idcode);
		gtk_frame_set_label(GTK_FRAME(mainWin->mFrameOrig), idcode);

		delete[] idcode;
		delete[] name;
		delete[] date;
		delete[] roll;
		delete[] location;
		delete[] shortdescription;

		//***1.96a - we want to use the mDataPos offsets to prevent
		// reloading the same fin (and rebuilding its modified image) unless
		// a real change of selected fin has occurred
		if (mainWin->mDBCurEntryOffset != mainWin->mSelectedFin->mDataPos)
		{
			// newly selected fin IS NOT the same fin as was previously selected
			// so remember offset and force reloading of fin image
			if (mainWin->mWindow->window != NULL) //***1.85
			{
				on_mainDrawingAreaImage_configure_event(mainWin->mDrawingAreaImage, NULL, userData); //***1.7
				on_mainDrawingAreaOrigImage_configure_event(mainWin->mDrawingAreaOrigImage, NULL, userData); //***1.7
				// configure call above reloads image, so remember the offset of the fin
				// with the currently loaded image, but do so AFTER the configure event
			}
			mainWin->mDBCurEntryOffset = mainWin->mSelectedFin->mDataPos; //***1.96a
			}
		}

	catch (Error e) {
		showError(e.errorString());
	}

	mainWin->refreshImage();
	mainWin->refreshOrigImage(); //***1.99
	mainWin->refreshOutline();
	
	// the following now ONLY updates the scrolling list to display correctly
	mainWin->setScrollWindowPosition(mainWin->mDBCurEntry);
}

//*******************************************************************
void on_mainButtonPrev_clicked(
	GtkButton *button,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	if (NULL == mainWin->mDatabase)
		return;
	
	GtkTreePath *path;
	GtkTreeIter iter;

	/* Get currently selected record. */
	gtk_tree_selection_get_selected(mainWin->mSelection, NULL, &iter);
	/* Get the current path from the iteration. */
	path = gtk_tree_model_get_path(mainWin->mModel, &iter);
	if (gtk_tree_path_prev(path)) {
		/* Try to move to the previous record! */
		if (gtk_tree_model_get_iter(mainWin->mModel, &iter, path))
			/* Select record. */
			gtk_tree_selection_select_iter(mainWin->mSelection, &iter);
	}
}

//*******************************************************************
void on_mainButtonNext_clicked(
	GtkButton *button,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	if (NULL == mainWin->mDatabase)
		return;

	GtkTreeIter iter;

	/* Get currently selected record. */
	gtk_tree_selection_get_selected(mainWin->mSelection, NULL, &iter);
	/* Try to move to the next record! */
	if (gtk_tree_model_iter_next(mainWin->mModel, &iter))
		/* Select record. */
		gtk_tree_selection_select_iter(mainWin->mSelection, &iter);

}

//*******************************************************************
void on_mainButtonModify_clicked(		//***002DB - nf
	GtkButton *button,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return;

	if (NULL == mainWin->mDatabase)
		return;

	ModifyDatabaseWindow *ModWin = new ModifyDatabaseWindow(
		mainWin->mDBCurEntry,
		mainWin,                  //***004CL
		mainWin->mSelectedFin,
		mainWin->mDatabase,
		mainWin->mOptions);
	ModWin->show();
}

//*******************************************************************
gboolean on_mainEventBoxImage_button_press_event(
	GtkWidget *widget,
	GdkEventButton *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	if (NULL == mainWin->mSelectedFin)
		return FALSE;

	// make sure the image exists before creating the ImageViewDialog
	//if (NULL == mainWin->mSelectedFin->mFinImage)
	//   mainWin->mSelectedFin->mFinImage  = new ColorImage(mainWin->mSelectedFin->mImageFilename);

	//ImageViewDialog *dlg = new ImageViewDialog(
	//		mainWin->mSelectedFin->mIDCode,
	//		mainWin->mSelectedFin->mFinImage);
	ImageViewDialog *dlg = new ImageViewDialog(
		mainWin->mWindow, //***2.22 - so dialog will be set transient for this window
		mainWin->mSelectedFin->mIDCode,
		mainWin->mImageFullsize); //***2.01 - use new fullsize image already in memory
	dlg->show();

	return TRUE;
}

//*******************************************************************
//***1.99 - new
//
gboolean on_mainEventBoxOrigImage_button_press_event(
	GtkWidget *widget,
	GdkEventButton *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	if (NULL == mainWin->mSelectedFin)
		return FALSE;

	// make sure the image exists before creating the ImageViewDialog
	//if (NULL == mainWin->mSelectedFin->mFinImage)
	//   mainWin->mSelectedFin->mFinImage  = new ColorImage(mainWin->mSelectedFin->mImageFilename);

	//ImageViewDialog *dlg = new ImageViewDialog(
	//		mainWin->mSelectedFin->mIDCode,
	//		mainWin->mSelectedFin->mFinImage); // was reloading and using modified image - OOPS
	ImageViewDialog *dlg = new ImageViewDialog(
		mainWin->mWindow, //***2.22 - so dialog will be set transient for this window
		mainWin->mSelectedFin->mIDCode,
		mainWin->mOrigImageFullsize); //***2.01 - use new fullsize image already in memory
	dlg->show();

	return TRUE;
}

//*******************************************************************
gboolean on_mainDrawingAreaImage_expose_event(
	GtkWidget *widget,
	GdkEventExpose *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	if (mainWin->mIndividualsTblRowCnt == 0){
		// Redraw a blank background
		cairo_t *cr = gdk_cairo_create(mainWin->mDrawingAreaImage->window);
		/* Set color */
		gdk_cairo_set_source_color(cr, &mainWin->mDrawingAreaImage->style->bg[GTK_STATE_NORMAL]);
		/* Set up our rectangle. */
		cairo_rectangle(cr, 0, 0,
			widget->allocation.width,
			widget->allocation.height);
		/* Fill the background.*/
		cairo_fill(cr);
		cairo_destroy(cr);
		return TRUE;
	}						//***002DB <

	//***1.85 - moved from above backgound redraw
	if (NULL == mainWin->mDrawingAreaImage || NULL == mainWin->mImage)
		return FALSE;

	gdk_draw_rgb_image(
		mainWin->mDrawingAreaImage->window,
		mainWin->mDrawingAreaImage->style->fg_gc[GTK_STATE_NORMAL],
		0, 0,
		mainWin->mImage->getNumCols(),
		mainWin->mImage->getNumRows(),
		GDK_RGB_DITHER_NONE,
		(guchar*)mainWin->mImage->getData(),
		mainWin->mImage->bytesPerPixel() * mainWin->mImage->getNumCols()
		);

	mainWin->updateCursor();
	return TRUE;
}

//*******************************************************************
gboolean on_mainDrawingAreaOrigImage_expose_event(
	GtkWidget *widget,
	GdkEventExpose *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	if (mainWin->mIndividualsTblRowCnt == 0){		//***002DB >
		// Redraw a blank background
		cairo_t *cr = gdk_cairo_create(mainWin->mDrawingAreaOrigImage->window);
		/* Set color */
		gdk_cairo_set_source_color(cr, &mainWin->mDrawingAreaOrigImage->style->bg[GTK_STATE_NORMAL]);
		/* Set up our rectangle. */
		cairo_rectangle(cr, 0, 0,
			widget->allocation.width,
			widget->allocation.height);
		/* Fill the background.*/
		cairo_fill(cr);
		cairo_destroy(cr);
		return TRUE;
	}						//***002DB <

	//***1.85 - moved from above backgound redraw
	if (NULL == mainWin->mDrawingAreaOrigImage || NULL == mainWin->mOrigImage)
		return FALSE;

	gdk_draw_rgb_image(
		mainWin->mDrawingAreaOrigImage->window,
		mainWin->mDrawingAreaOrigImage->style->fg_gc[GTK_STATE_NORMAL],
		0, 0,
		mainWin->mOrigImage->getNumCols(),
		mainWin->mOrigImage->getNumRows(),
		GDK_RGB_DITHER_NONE,
		(guchar*)mainWin->mOrigImage->getData(),
		mainWin->mOrigImage->bytesPerPixel() * mainWin->mOrigImage->getNumCols()
		);

	mainWin->updateCursor();
	return TRUE;
}

//*******************************************************************
gboolean on_mainEventBoxOutline_button_press_event(
	GtkWidget *widget,
	GdkEventButton *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	if (NULL == mainWin->mSelectedFin)
		return FALSE;

	if (getNumContourInfoDialogReferences() < 1) {
		ContourInfoDialog *dlg = new ContourInfoDialog(
			mainWin->mWindow, //***2.22
			mainWin->mSelectedFin->mIDCode,
			//mainWin->mSelectedFin->mFinContour, removed 008OL
			mainWin->mSelectedFin->mFinOutline, //***008OL
			mainWin->mOptions->mCurrentColor);
		dlg->show();
	}

	return TRUE;
}

//*******************************************************************
gboolean on_mainDrawingAreaOutline_expose_event(
	GtkWidget *widget,
	GdkEventExpose *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	if ((NULL == mainWin->mSelectedFin) && (mainWin->mIndividualsTblRowCnt > 0)) //***1.85 - size constraint
		return FALSE;

	// Redraw a blank background
	cairo_t *cr = gdk_cairo_create(mainWin->mDrawingAreaOutline->window);
	/* Set color */
	gdk_cairo_set_source_color(cr, &mainWin->mDrawingAreaOutline->style->bg[GTK_STATE_NORMAL]);
	/* Set up our rectangle. */
	cairo_rectangle(cr, 0, 0,
		mainWin->mCurContourWidth,
		mainWin->mCurContourHeight);
	/* Fill the background.*/
	cairo_fill(cr);
	cairo_destroy(cr);

	if (mainWin->mIndividualsTblRowCnt == 0)		//***002DB
		return TRUE;				//***002DB
	//Contour *c = mainWin->mSelectedFin->mFinContour; removed 008OL
	FloatContour *c = mainWin->mSelectedFin->mFinOutline->getFloatContour(); //***008OL

	int
		xMax = c->maxX(),
		yMax = c->maxY(),
		xMin = c->minX(),
		yMin = c->minY();

	int
		xRange = xMax - xMin,
		yRange = yMax - yMin;

	float
		heightRatio = mainWin->mCurContourHeight / ((float)yRange),
		widthRatio = mainWin->mCurContourWidth / ((float)xRange);

	float ratio;

	int xOffset = 0, yOffset = 0;

	if (heightRatio < widthRatio) {
		ratio = heightRatio;
		xOffset = (int)round((mainWin->mCurContourWidth - ratio * xRange) / 2);
	}
	else {
		ratio = widthRatio;
		yOffset = (int)round((mainWin->mCurContourHeight - ratio * yRange) / 2);
	}

	unsigned numPoints = c->length();
	cr = gdk_cairo_create(mainWin->mDrawingAreaOutline->window);
	/* Set color to black. */
	cairo_set_source_rgb(cr, 0, 0, 0);

	for (unsigned i = 0; i < numPoints; i++) {
		int xCoord = (int)round(((*c)[i].x - xMin) * ratio + xOffset);
		int yCoord = (int)round(((*c)[i].y - yMin) * ratio + yOffset);

		/* Set up our rectangle. */
		cairo_rectangle(cr,
			xCoord - POINT_SIZE / 2,
			yCoord - POINT_SIZE / 2,
			POINT_SIZE,
			POINT_SIZE);

		/* Fill the rectangle.*/
		cairo_fill(cr);

	}
	cairo_destroy(cr);

	mainWin->updateCursor();
	return TRUE;
}

//*******************************************************************
gboolean on_mainDrawingAreaOutline_configure_event(
	GtkWidget *widget,
	GdkEventConfigure *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	mainWin->mCurContourHeight = widget->allocation.height;
	mainWin->mCurContourWidth = widget->allocation.width;

	mainWin->updateGC(); //***1.99 - in cae notebook page was hidden initially

	mainWin->refreshOutline();

	return TRUE;
}

//*******************************************************************
gboolean on_mainDrawingAreaImage_configure_event(
	GtkWidget *widget,
	GdkEventConfigure *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	//mainWin->mCurImageHeight = widget->allocation.height;
	//mainWin->mCurImageWidth = widget->allocation.width;

	if (NULL == mainWin->mSelectedFin)
		return TRUE;

	//***1.96a - no change in image so no need to reconfigure (reload image)
	// second condition makes sure this was called by us on row selection
	// and NOT by an actual window size/state change
	if ((mainWin->mSelectedFin->mDataPos == mainWin->mDBCurEntryOffset) &&
		(event == NULL))
		return TRUE;

	delete mainWin->mImage;

	// since resizeWithBorder creates a new image, we must delete this one after it is used
	ColorImage *tempImage = new ColorImage(mainWin->mSelectedFin->mImageFilename);

	if (NULL != mainWin->mImageFullsize) //***2.01
		delete mainWin->mImageFullsize;  //***2.01

	mainWin->mImage = resizeWithBorder(
		tempImage,
		//mainWin->mCurImageHeight,
		//mainWin->mCurImageWidth);
		mainWin->mDrawingAreaImage->allocation.height, //***1.7
		mainWin->mDrawingAreaImage->allocation.width); //***1.7

	//***1.99 - since the resizeWithBorder DOES NOT preserve any image
	// attributes except the basi image pixel data, we must copy the info
	// over here -- this should be fixed in the image classes and their support
	// functions - JHS

	mainWin->mImage->mOriginalImageFilename = tempImage->mOriginalImageFilename;
	mainWin->mImage->mNormScale = tempImage->mNormScale; // this may not be usefull
	mainWin->mImage->mImageMods = tempImage->mImageMods;

	//***1.8 - if information available, set up FRAME label to indicate any image reversal
	gchar *frameLabel = new gchar[mainWin->mSelectedFin->mIDCode.length() + 12]; // leave room for " (reversed)"
	strcpy(frameLabel, mainWin->mSelectedFin->mIDCode.c_str());
	if (tempImage->mImageMods.imageIsReversed())
		strcat(frameLabel, " (reversed)");
	gtk_frame_set_label(GTK_FRAME(mainWin->mFrameMod), frameLabel);
	delete[] frameLabel;
	//***1.8 - end

#ifdef DEBUG
	cout << "Scale: " << tempImage->mNormScale << endl;

	//***1.9 - check validity of image mods read from file
	if (0 < tempImage->mImageMods.size())
	{
		ImageMod mod(ImageMod::IMG_none);

		ImageMod::ImageModType op;
		int v1, v2, v3, v4;

		tempImage->mImageMods.first(mod);
		mod.get(op, v1, v2, v3, v4);
		cout << "ImgMod: " << (int)op << " " << v1 << " " << v2 << " " << v3 << " " << v4 << endl;

		while (tempImage->mImageMods.next(mod))
		{
			mod.get(op, v1, v2, v3, v4);
			cout << "ImgMod: " << (int)op << " " << v1 << " " << v2 << " " << v3 << " " << v4 << endl;
		}
	}
#endif

	// delete here or memory leak results
	//delete tempImage;
	mainWin->mImageFullsize = tempImage; //***2.01 - deleted as needed above and in destructor now
	mainWin->refreshImage();

	return TRUE;
}

//*******************************************************************
gboolean on_mainDrawingAreaOrigImage_configure_event(
	GtkWidget *widget,
	GdkEventConfigure *event,
	gpointer userData)
{
	MainWindow *mainWin = (MainWindow *)userData;

	if (NULL == mainWin)
		return FALSE;

	if (NULL == mainWin->mSelectedFin)
		return TRUE;

	//***1.96a - no change in image so no need to reconfigure (reload image)
	// second condition makes sure this was called by us on Clist row selection
	// and NOT by an actual window size/state change
	if ((mainWin->mSelectedFin->mDataPos == mainWin->mDBCurEntryOffset) &&
		(event == NULL))
		return TRUE;

	// the only way we know the original image name is if the modifed image
	// is already loaded, so if not we bail
	if (NULL == mainWin->mImage)
		return FALSE;

	// if the original image name is not available, then mImage is not
	// a darwin modified image, so again we bail
	if ("" == mainWin->mImage->mOriginalImageFilename)
		return FALSE;

	// set the selected fin's original filename
	string path = mainWin->mSelectedFin->mImageFilename;
	path = path.substr(0, path.rfind(PATH_SLASH) + 1); // don't strip slash
	mainWin->mSelectedFin->mOriginalImageFilename =
		path +
		mainWin->mImage->mOriginalImageFilename; // short name

	//cout << "ORIG: " << mainWin->mSelectedFin->mOriginalImageFilename << endl;

	delete mainWin->mOrigImage;

	// since resizeWithBorder creates a new image, we must delete this one after it is used
	ColorImage *tempImage = new ColorImage(mainWin->mSelectedFin->mOriginalImageFilename);

	if (NULL != mainWin->mOrigImageFullsize) //***2.01
		delete mainWin->mOrigImageFullsize;  //***2.01

	mainWin->mOrigImage = resizeWithBorder(
		tempImage,
		mainWin->mDrawingAreaOrigImage->allocation.height, //***1.7
		mainWin->mDrawingAreaOrigImage->allocation.width); //***1.7

	//indicat that this is the original image
	gchar *frameLabel = new gchar[mainWin->mSelectedFin->mIDCode.length() + 12]; // leave room for " (reversed)"
	strcpy(frameLabel, mainWin->mSelectedFin->mIDCode.c_str());
	strcat(frameLabel, " (original)");
	gtk_frame_set_label(GTK_FRAME(mainWin->mFrameOrig), frameLabel);
	delete[] frameLabel;

	// delete here or memory leak results
	//delete tempImage;
	mainWin->mOrigImageFullsize = tempImage; //***2.01 - deleted as needed above and in destructor now

	mainWin->refreshOrigImage();

	return TRUE;
}

