//
//   file: ExportFinzDialog.h
//
// author: J H Stewman
//
//   date: 7/22/2008
//

#ifndef EXPORTFINZDIALOG_H
#define EXPORTFINZDIALOG_H

#include <set>
#include <vector>
#include "../CatalogSupport.h"

class ExportFinzDialog {
	public:
		//***2.02 - new enum so we can use dialog in two different modes
		enum {
			exportFinz=0,
			exportFullSizeModImages 
		};

		ExportFinzDialog(Database *db, GtkWidget *parent,
			GtkWidget *view, int dialogMode); //***2.02 -new parameter

		~ExportFinzDialog();

		std::set<long> num_selected_rows; // Number of selected rows.

		void show();
		void setScrollWindowPosition(int newCurEntry);
		void displayStatusMessage(const std::string &msg);

		friend void on_finzDialog_delete_event(
				GtkWidget *widget,
				GdkEvent *event,
				gpointer userData);

		friend void on_finzDialogButtonSaveFinz_clicked(
				GtkButton *button,
				gpointer userData);

		friend void on_finzDialogButtonSaveImages_clicked( //***2.02
				GtkButton *button,
				gpointer userData);

		friend void on_finzDialogButtonSaveAsCatalog_clicked(
				GtkButton *button,
				gpointer userData);

		friend void on_finzDialogButtonCancel_clicked(
				GtkButton *button,
				gpointer userData);

		friend void on_ExportTreeView_select_row(
			GtkTreeSelection *selection,
			gpointer userData);

private:
		// Set up Tree store to be used with GtkTreeView.
		GtkTreeStore *mExportStore;
		GtkWidget *mExportView;
		GtkTreeModel *mExportModel;
		GtkTreeViewColumn *mExportColumn;
		GtkCellRenderer *mExportRenderer;
		GtkTreeIter *mExportIter;
		GtkTreeSelection *mExportSelection;

		// For use in gtk_tree_store_new()
		enum
		{
			PIXBUFF_COLUMN,
			USER_SUPPLIED_ID_COLUMN,
			NAME_COLUMN,
			DAMAGE_COLUMN,
			DATE_COLUMN,
			LOCATION_COLUMN,
			INDV_ID_COLUMN,
			N_COLUMNS
		};

		GtkWidget
			*mDialog,
			*mParentWindow,
			*mScrollable,
			*mButtonSave,
			*mButtonSaveAsCatalog,
			*mButtonCancel,
			*mStatusBar;

		std::set<long>  //***2.22 - now 64 bit arch on Mac
			mDBCurEntry; // supports multiple selection

		bool 
			mShowAlternates;

		int
			mDialogMode; //***2.02 - we are EITHER exporting Finz OR saving full-size images

		guint mContextID;

		DatabaseFin<ColorImage> 
			*mSelectedFin;

		Database 
			*mDatabase;

		ColorImage 
			*mImage,
			*mOrigImage;
		int
			mCurImageHeight, 
			mCurImageWidth,
			mCurContourHeight, 
			mCurContourWidth;

		GdkGC 
			*mGC;

		GdkCursor 
			*mCursor;

		Options 
			*mOptions;

		std::string
			mImportFromFilename,
			mExportToFilename;

		GtkWidget* createDialog();
		void copyMainWindowList(GtkWidget *view);  //KLY
};

#endif
