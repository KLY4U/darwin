//
//   file: ExportFinzDialog.cxx
//
// author: J H Stewman
//
//   date: 7/22/2008
//
// This class provides the user interface for the export of multiple
// *.FinZ files.  The output format may either be a single FinZ file,
// a collection of several FinZ files or an actual Catalog which is a
// SUBSET of the currently open DARWIN catalog.
//
// In versions 2.02 and later it also supports saving of full-size, 
// full-resolution versions of DARWIN modified images that are otherwise
// saved as thumbnails inside the catalog folder and are rebuilt
// as full-size images on-the-fly whenever DARWIN needs to access
// them.  JHS 7/25/2009

#include "../thumbnail.h"
#include "ExportFinzDialog.h"
#ifndef WIN32
#pragma GCC diagnostic ignored "-Wwrite-strings"
#endif
#include "../../pixmaps/add_database.xpm"
#include "../../pixmaps/cancel.xpm"
#include "../../pixmaps/open_trace.xpm"
#include "SaveFileChooserDialog.h"

static const char *NONE_SUBSTITUTE_STRING = _("(Not Entered)");

static int anchorRow = -1;

using namespace std;


//-------------- support function for determining selections ------------

/* A function used by gtk_tree_selection_selected_foreach() to map all selected rows. It will be called on every selected row in the view. */
gboolean view_selection_func(GtkTreeModel *model,
									GtkTreePath *path,
									GtkTreeIter *iter,
									gpointer userdata)
{
	ExportFinzDialog *dlg = (ExportFinzDialog *)userdata;

	long *id;

	/* Get the individuals ID (column 6) from our selected row. */
	gtk_tree_model_get(model, iter, 6, &id, -1);
	/* Add the ID in dlg->num_selected_rows. */
	dlg->num_selected_rows.insert((long)id);
	
	return true;
}

//----------------------- the member functions -------------------------

//*******************************************************************
ExportFinzDialog::ExportFinzDialog(Database *db, GtkWidget *parent, 
									GtkWidget *view, int dialogMode) //***2.02 - new operating mode parameter
:	mDialog(NULL),
	mDatabase(db),
	mParentWindow(parent),
	mShowAlternates(false), // for now
	mDialogMode(dialogMode), //***2.02
	mExportView(NULL), // KLY
	mExportStore(NULL), // KLY
	mExportModel(NULL), // KLY
	mExportIter(), // KLY
	mExportSelection(NULL) // KLY

{
	mDBCurEntry.clear();
	mDialog = createDialog();
	copyMainWindowList(view);
}

//*******************************************************************
ExportFinzDialog::~ExportFinzDialog()
{
	if (NULL != mDialog)
		gtk_widget_destroy(mDialog);

}

//*******************************************************************
void ExportFinzDialog::show()
{
	gtk_widget_show(mDialog);
}

//*******************************************************************
void ExportFinzDialog::copyMainWindowList(GtkWidget *view)
{
	// this copies the list passed in from the MainWindow
	// assume this is called AFTER createDialog() in constructor
	try {
		// Some variables for our TreeView.
		GtkTreeIter MainWindowIter, ExportIter, MainWindowSelectedRecIter;
		gint sort_column_id;
		int *rowpos;
		GtkSortType sort_order;
		GtkTreePath *path;
		gboolean valid;
		GdkPixbuf *pixbuf = NULL;
		boolean MainWindowRecordSelected = false;

		// Pointers back to our MainWindow store.
		MainWindow *mainWin = (MainWindow *)mParentWindow;
		GtkWidget *mMainWindowView = view;
		GtkTreeModel *mMainWindowModel = gtk_tree_view_get_model(GTK_TREE_VIEW(mMainWindowView));
		GtkTreeStore *mMainWindowStore = GTK_TREE_STORE(mMainWindowModel);
		GtkTreeSelection *mMainWindowSelection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mMainWindowView));

		/* Speed Issues when Adding a Lot of Rows
		A common scenario is that a model needs to be filled with a lot of rows at some point,
		either at start-up, or when some file is opened. An equally common scenario is that this
		takes an awfully long time even on powerful machines once the model contains more than
		a couple of thousand rows, with an exponentially decreasing rate of insertion.
		Writing a custom model might be the best thing to do in this case.
		Nevertheless, there are some things you can do to work around this problem and speed things
		up a bit even with the stock Gtk+ models:

		Firstly, you should detach your list store or tree store from the tree view before doing
		your mass insertions, then do your insertions, and only connect your store to the tree view
		again when you are done with your insertions.

		Secondly, you should make sure that sorting is disabled while you are doing your mass insertions,
		otherwise your store might be resorted after each and every single row insertion, which is going
		to be everything but fast.

		Thirdly, you should not keep around a lot of tree row references if you have so many rows,
		because with each insertion (or removal) every single tree row reference will check whether its
		path needs to be updated or not.
		*/

		/* Create a new model for the Export Window */
		mExportModel = gtk_tree_view_get_model(GTK_TREE_VIEW(mExportView));
		/* Get the store. */
		mExportStore = GTK_TREE_STORE(mExportModel);
		/* Remove all rows from the store. */
		gtk_tree_store_clear(mExportStore);
		// Store current sorting information and store in &sort_column_id, &sort_order.
		gtk_tree_sortable_get_sort_column_id(GTK_TREE_SORTABLE(mMainWindowModel), &sort_column_id, &sort_order);
		/* Set sort order to unsorted. */
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(mExportModel), GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID,
			GTK_SORT_ASCENDING);
		/* Initalize mExportSelection */
		mExportSelection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mExportView));
		/* Multiple items are not allowed */
		gtk_tree_selection_set_mode(mExportSelection, GTK_SELECTION_SINGLE);
		/* Make sure the model stays with us after the tree view unrefs it */
		g_object_ref(mExportStore);
		/* Detach model from view */
		gtk_tree_view_set_model(GTK_TREE_VIEW(mExportView), NULL);

		/* Now start adding your rows to the Export store. */
		cout << "Creating Export List From Main Window List" << endl;

		/* Get the first iter in the Main Window list */
		valid = gtk_tree_model_get_iter_first(mMainWindowModel, &MainWindowIter);

		gchar *user_id, *indv_id, *name, *damage, *date, *location;
		while (valid)
		{
			/* Walk through the list, reading each row */
			gtk_tree_model_get(mMainWindowModel, &MainWindowIter,
				PIXBUFF_COLUMN, &pixbuf,
				USER_SUPPLIED_ID_COLUMN, &user_id,
				NAME_COLUMN, &name,
				DAMAGE_COLUMN, &damage,
				DATE_COLUMN, &date,
				LOCATION_COLUMN, &location,
				INDV_ID_COLUMN, &indv_id,
				-1);

			// Get position of appended record.
			gtk_tree_store_append(mExportStore, &ExportIter, NULL);

			gtk_tree_store_set(mExportStore, &ExportIter,
				PIXBUFF_COLUMN, pixbuf,
				INDV_ID_COLUMN, indv_id,
				USER_SUPPLIED_ID_COLUMN, user_id,
				NAME_COLUMN, name,
				DAMAGE_COLUMN, damage,
				DATE_COLUMN, date,
				LOCATION_COLUMN, location,
				-1);

			/* If main window record is selected, then select this record in the export dialog. */
			if (gtk_tree_selection_iter_is_selected(mMainWindowSelection, &MainWindowIter)) {
				MainWindowRecordSelected = true;

				/* Save this path so we can select it later, after we re-attach the model. */
				path = gtk_tree_model_get_path(mExportModel, &ExportIter);
			}

			/* Get the next iter in the list */
			valid = gtk_tree_model_iter_next(mMainWindowModel, &MainWindowIter);
		}
			
		cout << endl << "Set the tree sort order." << endl;
		gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(mExportModel), sort_column_id, sort_order);

		cout << endl << "Re-attach model to view." << endl;
		gtk_tree_view_set_model(GTK_TREE_VIEW(mExportView), GTK_TREE_MODEL(mExportStore));
		g_object_unref(mExportStore);

		if (MainWindowRecordSelected) {
			cout << endl << "Select our record." << endl;
			gtk_tree_selection_select_path(mExportSelection, path);
			gtk_tree_path_free(path);
		} else if (gtk_tree_model_get_iter_first(mExportModel, &ExportIter)) {
				cout << endl << "Move to first row in list." << endl;
				path = gtk_tree_model_get_path(mExportModel, &ExportIter);

				cout << endl << "Select our record." << endl;
				gtk_tree_selection_select_path(mExportSelection, path);
		}

		cout << endl << "Resize all columns to their optimal width." << endl;
		gtk_tree_view_columns_autosize(GTK_TREE_VIEW(mExportView));

		cout << endl << "List creation complete" << endl;

	}
	catch (Error e) {
		showError(_("The database seems to be corrupted.\n"
			"Some (or all) entries may not appear\n"
			"correctly."));
	}

}

//*******************************************************************
//
// Function simply adjusts the scrolling window so that the selected fin
// is visible.  Size of the model is indicated by the number of rows in the model.
//
void ExportFinzDialog::setScrollWindowPosition(int newCurEntry)       //***004CL
{
	/* Returns the number of rows in the model. */
	gint n_rows = gtk_tree_model_iter_n_children(mExportModel, NULL);

	if (n_rows == 0)
		return;

	// else force scrollable window to scroll down to selected row

	const int lineHeight = DATABASEFIN_THUMB_HEIGHT + 2;
	static int lastEntry = 0;

	int pageEntries = mScrollable->allocation.height / lineHeight;

	GtkAdjustment *adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(mScrollable));
	int topEntry = (int)(adj->value) / lineHeight;

	//g_print("top/last/this = (%d %d %d)\n", topEntry, lastEntry,newCurEntry);

	if ((newCurEntry > topEntry + pageEntries - 3) || (newCurEntry < topEntry))
	{
		// NOTE: the (-2) in the test above prevents highlighting of partially visible
		// item at bottom of page when pressing NEXT button

		if ((lastEntry + 1 == newCurEntry) &&
			(newCurEntry > topEntry) && (newCurEntry == topEntry + pageEntries - 2))
			adj->value += lineHeight; // just scroll down one line
		else
		{
			/* Rows that had their ID Code changed were showing up hidden at the top of the window.  Correction to follow. */
			if (newCurEntry > 1)
				newCurEntry = newCurEntry - 1;

			// reposition list so newCurEntry is at top
			float where = (double)newCurEntry / n_rows; //***1.96
			adj->value = where * (adj->upper - adj->lower);
			topEntry = newCurEntry;
		}

		// this should be called but seems to be meaningless and scroll update
		// works without it - JHS
		//gtk_scrolled_window_set_vadjustment(GTK_SCROLLED_WINDOW(mScrollable),adj);

		gtk_adjustment_value_changed(adj);
	}
	lastEntry = newCurEntry;
}


//*******************************************************************
void ExportFinzDialog::displayStatusMessage(const string &msg)
{
	gtk_statusbar_pop(GTK_STATUSBAR(mStatusBar), mContextID);
	gtk_statusbar_push(GTK_STATUSBAR(mStatusBar), mContextID, msg.c_str());
}

//------------------------- createDialog -------------------------------

GtkWidget* ExportFinzDialog::createDialog()
{
	GtkWidget *mainWindow;
	GtkWidget *mainVBox;
	GtkWidget *mainHPaned;
	GtkWidget *mainLeftVBox;
	GtkWidget *mainExportWindow;

	GtkWidget *hbuttonbox1;
	GtkWidget *mainInfoTable;
	GtkWidget *mainRightVBox;
	GtkTooltips *tooltips;
	GtkWidget *tmpLabel, *tmpIcon, *tmpBox;
	GtkWidget *leftFrame, *rightFrame;
	set<long> rows; // Number of selected rows in our Export list.


	//string MainWinTitle = "DARWIN - Export FINZ";
	string MainWinTitle; //***2.02 - set title below based on mode
	switch (mDialogMode)
	{
	case exportFinz:
		MainWinTitle = "DARWIN - Export FINZ";
		break;
	case exportFullSizeModImages:
		MainWinTitle = "DARWIN - Export full-size IMAGES";
		break;
	default:
		MainWinTitle = "DARWIN - Dialog Unknown Mode Error";
		break;
	}
	//***1.85 - set the database filename / message for the window
	switch (mDatabase->status())
	{
	case Database::loaded :
		MainWinTitle += " (from " + mDatabase->getFilename() + ")";
		break;
	default :
		MainWinTitle += " (unknown error with database file)";
		break;
	}

	tooltips = gtk_tooltips_new ();


	mDialog = gtk_dialog_new ();
	g_object_set_data (G_OBJECT(mDialog), "exportDialog", mDialog);
	gtk_window_set_title (GTK_WINDOW (mDialog), _(MainWinTitle.c_str()));
	GTK_WINDOW (mDialog)->type = WINDOW_DIALOG;
	gtk_window_set_position (GTK_WINDOW (mDialog), GTK_WIN_POS_CENTER);
	gtk_window_set_wmclass(GTK_WINDOW(mDialog), "darwin_export_finz", "DARWIN");
	gtk_window_set_default_size(GTK_WINDOW(mDialog), 500, 600);

	gtk_window_set_modal(GTK_WINDOW(mDialog), TRUE); //***1.3
	gtk_window_set_transient_for(GTK_WINDOW(mDialog), GTK_WINDOW(mParentWindow));

	mainVBox = GTK_DIALOG(mDialog)->vbox;

	// set up rest of main window

	mainHPaned = gtk_hpaned_new();
	gtk_widget_show(mainHPaned);
	gtk_box_pack_start(GTK_BOX(mainVBox), mainHPaned, TRUE, TRUE, 0);

	leftFrame = gtk_frame_new(NULL);
	gtk_widget_show(leftFrame);
	gtk_frame_set_shadow_type(GTK_FRAME(leftFrame), GTK_SHADOW_IN);
	gtk_paned_pack1(GTK_PANED(mainHPaned), leftFrame, TRUE, TRUE);

	//***1.96 - new box for view list options
	mainLeftVBox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (mainLeftVBox);
	gtk_container_add(GTK_CONTAINER(leftFrame), mainLeftVBox);
	gtk_container_set_border_width (GTK_CONTAINER (mainLeftVBox), 4);

	mainExportWindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_show(mainExportWindow);
	gtk_box_pack_start(GTK_BOX(mainLeftVBox), mainExportWindow, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(mainExportWindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

	mScrollable = mainExportWindow; //***1.7CL

	// Configure GtkTreeView.  Mod by KLY.
	// http://maemo.org/api_refs/5.0/5.0-final/gtk/TreeWidget.html

	/* Create a model.  */
	mExportStore = gtk_tree_store_new(N_COLUMNS,
		GDK_TYPE_PIXBUF, /* PIXBUFF_COLUMN */
		G_TYPE_STRING, /* USER_SUPPLIED_ID_COLUMN*/
		G_TYPE_STRING, /* NAME_COLUMN */
		G_TYPE_STRING, /* DAMAGE_COLUMN*/
		G_TYPE_STRING, /* DATE_COLUMN */
		G_TYPE_STRING, /* LOCATION_COLUMN */
		G_TYPE_INT /* INDV_ID_COLUMN*/
		);


	/* Create a view */
	mExportView = gtk_tree_view_new_with_model(GTK_TREE_MODEL(mExportStore));

	/* Create a column, associating the "pixbuf" attribute of the  cell_renderer to the first column of the model	*/
	/* https://mail.gnome.org/archives/gtk-app-devel-list/2003-June/msg00026.html */
	mExportRenderer = gtk_cell_renderer_pixbuf_new();
	mExportColumn = gtk_tree_view_column_new_with_attributes("", mExportRenderer, "pixbuf", PIXBUFF_COLUMN, NULL);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mExportView), mExportColumn);

	/* Create a column, associating the "text" attribute of the cell_renderer to this column of the model	*/
	mExportRenderer = gtk_cell_renderer_text_new();
	mExportColumn = gtk_tree_view_column_new_with_attributes("ID", mExportRenderer, "text", USER_SUPPLIED_ID_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mExportColumn, USER_SUPPLIED_ID_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mExportView), mExportColumn);

	mExportRenderer = gtk_cell_renderer_text_new();
	mExportColumn = gtk_tree_view_column_new_with_attributes("Name", mExportRenderer, "text", NAME_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mExportColumn, NAME_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mExportView), mExportColumn);

	mExportRenderer = gtk_cell_renderer_text_new();
	mExportColumn = gtk_tree_view_column_new_with_attributes("Damage", mExportRenderer, "text", DAMAGE_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mExportColumn, DAMAGE_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mExportView), mExportColumn);

	mExportRenderer = gtk_cell_renderer_text_new();
	mExportColumn = gtk_tree_view_column_new_with_attributes("Date", mExportRenderer, "text", DATE_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mExportColumn, DATE_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mExportView), mExportColumn);

	mExportRenderer = gtk_cell_renderer_text_new();
	mExportColumn = gtk_tree_view_column_new_with_attributes("Location", mExportRenderer, "text", LOCATION_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mExportColumn, LOCATION_COLUMN);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mExportView), mExportColumn);

	mExportRenderer = gtk_cell_renderer_text_new();
	mExportColumn = gtk_tree_view_column_new_with_attributes("INDV ID", mExportRenderer, "text", INDV_ID_COLUMN, NULL);
	gtk_tree_view_column_set_sort_column_id(mExportColumn, INDV_ID_COLUMN);
	gtk_tree_view_column_set_visible(mExportColumn, FALSE);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mExportView), mExportColumn);

	/* Initialize our selection so that the g_signal_connect is created successfully. */
	mExportSelection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mExportView));

	/* The view now holds a reference.  We can get rid of our own reference */
	g_object_unref(mExportStore);

	// Allow the column title buttons to be clicked.
	gtk_tree_view_set_headers_clickable(GTK_TREE_VIEW(mExportView), true);
	// Sets the visibility state of the headers.
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(mExportView), true);

	gtk_container_add(GTK_CONTAINER(mainExportWindow), mExportView);
	gtk_widget_show(mExportView);

	//***1.85 - new FindDolphin by ID tool
	//***1.95 - moved from above Clist to below
	GtkWidget *tempHbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start (GTK_BOX (mainLeftVBox), tempHbox, FALSE, FALSE, 5);
	gtk_widget_show(tempHbox);

	GtkWidget *findLabel = gtk_label_new("To Find Dolphin by ID, click ID column and type ID");
	gtk_box_pack_start (GTK_BOX (tempHbox), findLabel, FALSE, FALSE, 0);
	gtk_widget_show(findLabel);

	/* Disable use of find entry box and use built-in search function.  Mod by KLY
	GtkWidget *findEntry = gtk_entry_new();
	gtk_box_pack_start (GTK_BOX (tempHbox), findEntry, FALSE, FALSE, 0);
	gtk_widget_show(findEntry);

	mSearchID = findEntry;

	GtkWidget *findNow = gtk_button_new_with_label("Goto");
	gtk_box_pack_start (GTK_BOX (tempHbox), findNow, FALSE, FALSE, 0);
	gtk_widget_show(findNow);
	*/

	// create button box with "SaveFin(s)", "SaveAsCatalog" & "Cancel" buttons

	hbuttonbox1 = gtk_hbutton_box_new();
	gtk_widget_show(hbuttonbox1);
	gtk_box_pack_start(GTK_BOX(mainLeftVBox), hbuttonbox1, FALSE, TRUE, 5);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(hbuttonbox1), GTK_BUTTONBOX_END);
    gtk_button_box_set_spacing(GTK_BUTTON_BOX(hbuttonbox1), 8);

	// create Cancel button

	mButtonCancel = gtk_button_new();

	tmpBox = gtk_hbox_new(FALSE, 0);			
	tmpIcon = create_pixmap_from_data(tmpBox, cancel_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_Cancel"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mButtonCancel);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, TRUE, TRUE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);

	gtk_container_add(GTK_CONTAINER(mButtonCancel), tmpBox);

	gtk_widget_show(mButtonCancel);
	//***1.99 - this button goes with the data in the new notebook page below
	gtk_container_add (GTK_CONTAINER (hbuttonbox1), mButtonCancel);
	GTK_WIDGET_SET_FLAGS (mButtonCancel, GTK_CAN_DEFAULT);
	gtk_tooltips_set_tip (tooltips, mButtonCancel, _("Cancel without saving any FinZ."), NULL);

	// create Save button

	mButtonSave = gtk_button_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, open_trace_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	if (mDialogMode == exportFinz)
		tmpLabel = gtk_label_new_with_mnemonic(_("_Save Fin(s)"));
	else if (mDialogMode == exportFullSizeModImages)
		tmpLabel = gtk_label_new_with_mnemonic(_("_Save Image(s)"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mButtonSave);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, TRUE, TRUE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);
  
	gtk_container_add(GTK_CONTAINER(mButtonSave), tmpBox);

	gtk_widget_show(mButtonSave);
	gtk_container_add(GTK_CONTAINER(hbuttonbox1), mButtonSave);
	GTK_WIDGET_SET_FLAGS(mButtonSave, GTK_CAN_DEFAULT);
	if (mDialogMode == exportFinz)
		gtk_tooltips_set_tip(tooltips, mButtonSave, 
	                     _("Save selected fin(s) as *.finz files."), NULL);
	else if (mDialogMode == exportFullSizeModImages)
		gtk_tooltips_set_tip(tooltips, mButtonSave, 
	                     _("Save image(s) of selected fin(s) as *.png files."), NULL);


	// create SaveAsCatalog button

	mButtonSaveAsCatalog = gtk_button_new();

	tmpBox = gtk_hbox_new(FALSE, 0);
	tmpIcon = create_pixmap_from_data(tmpBox, add_database_xpm);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpIcon, FALSE, FALSE, 0);
	gtk_widget_show(tmpIcon);
	tmpLabel = gtk_label_new_with_mnemonic(_("_Save as Catalog"));
	gtk_label_set_mnemonic_widget(GTK_LABEL(tmpLabel), mButtonSaveAsCatalog);
	gtk_box_pack_start(GTK_BOX(tmpBox), tmpLabel, TRUE, TRUE, 0);
	gtk_widget_show(tmpLabel);
	gtk_widget_show(tmpBox);
  
	gtk_container_add(GTK_CONTAINER(mButtonSaveAsCatalog), tmpBox);

	// for now, this option does not exist -- JHS
	//gtk_widget_show(mButtonSaveAsCatalog);
	gtk_container_add (GTK_CONTAINER (hbuttonbox1), mButtonSaveAsCatalog);
	GTK_WIDGET_SET_FLAGS (mButtonSaveAsCatalog, GTK_CAN_DEFAULT);
	gtk_tooltips_set_tip (tooltips, mButtonSaveAsCatalog, _("Save selected fin(s) as a new catalog."), NULL);


	//***1.99 - the right frame will now contain a TABBED area for the
	// modified image, original image, data fields, outline, ...

	// for now this area is NOT USED - JHS

	rightFrame = gtk_frame_new(NULL);
	gtk_widget_show(rightFrame);
	gtk_frame_set_shadow_type(GTK_FRAME(rightFrame), GTK_SHADOW_IN);
	gtk_paned_pack2(GTK_PANED(mainHPaned), rightFrame, TRUE, TRUE);
  
	mainRightVBox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (mainRightVBox);
	gtk_container_add(GTK_CONTAINER(rightFrame), mainRightVBox);


	// FINALLY, at bottom of main window is a status bar
	
	mStatusBar = gtk_statusbar_new ();
	gtk_widget_show(mStatusBar);

	mContextID = gtk_statusbar_get_context_id(GTK_STATUSBAR(mStatusBar), "mainWin");

	gtk_box_pack_start(GTK_BOX(mainVBox), mStatusBar, FALSE, FALSE, 0);

	g_signal_connect (G_OBJECT (mDialog), "delete_event",
	                    GTK_SIGNAL_FUNC (on_finzDialog_delete_event),
	                    (void *) this);

	if (mDialogMode == exportFinz)
		g_signal_connect (G_OBJECT (mButtonSave), "clicked",
	                    GTK_SIGNAL_FUNC (on_finzDialogButtonSaveFinz_clicked),
	                    (void *) this);
	else if (mDialogMode == exportFullSizeModImages)
		g_signal_connect (G_OBJECT (mButtonSave), "clicked",
	                    GTK_SIGNAL_FUNC (on_finzDialogButtonSaveImages_clicked),
	                    (void *) this);

	g_signal_connect (G_OBJECT (mButtonSaveAsCatalog), "clicked",
	                    GTK_SIGNAL_FUNC (on_finzDialogButtonSaveAsCatalog_clicked),
	                    (void *) this);

	g_signal_connect (G_OBJECT (mButtonCancel), "clicked",	//***002DB
	                    GTK_SIGNAL_FUNC (on_finzDialogButtonCancel_clicked),
	                    (void *) this);

	g_signal_connect (G_OBJECT(mExportSelection), "changed",
						GTK_SIGNAL_FUNC(on_ExportTreeView_select_row),
						(void *) this);

	gtk_widget_grab_default(mButtonCancel);
	g_object_set_data (G_OBJECT (mDialog), "tooltips", tooltips);

	return mDialog;
}

//------------------------- the friend functions -----------------------
//*******************************************************************
//
void on_ExportTreeView_select_row(
	GtkTreeSelection *selection,
	gpointer userData)
{
	ExportFinzDialog *dlg = (ExportFinzDialog *)userData;

	if (NULL == dlg)
		return;

	/* Clear the current num_selected_rows "set" from the Export dialog. */
	dlg->num_selected_rows.clear();
	/* Call the view_selection_func to add all selected individuals ID to num_selected_rows "set". */
	gtk_tree_selection_selected_foreach(selection, (GtkTreeSelectionForeachFunc) view_selection_func, userData);
}

//*******************************************************************
//
void on_finzDialog_delete_event(
		GtkWidget *widget,
		GdkEvent *event,
		gpointer userData)
{
	// essentially the same as a CANCEL button click

	ExportFinzDialog *dlg = (ExportFinzDialog *) userData;

	if (NULL == dlg)
		return;

	delete dlg;
}

//*******************************************************************
//
void on_finzDialogButtonSaveFinz_clicked(
		GtkButton *button,
		gpointer userData)
{
	ExportFinzDialog *dlg = (ExportFinzDialog *) userData;

	if (NULL == dlg)
		return;

	bool saved = false;

	/* dlg->num_selected_rows is a "set" of individuals IDs. */
	set<long> selectedFins = dlg->num_selected_rows;

	if (selectedFins.empty()) {//idiot, selected something first for export
		return;
	} else if(selectedFins.size() == 1) {
		set<long>::iterator it = selectedFins.begin();
		DatabaseFin<ColorImage>* fin;
		/* Get our fin based upon *it, which is the ID. */
		fin = dlg->mDatabase->getItemAbsolute(*it);

		SaveFileChooserDialog *fsChooserDlg = new SaveFileChooserDialog(dlg->mDatabase,
												fin,
												NULL,
												NULL,
												dlg->mOptions,
												dlg->mDialog,
												SaveFileChooserDialog::saveFin);
		saved=fsChooserDlg->run_and_respond();
		
		delete fin;

	} else {//more than one selected
		set<long>::iterator it;
		vector<DatabaseFin<ColorImage>* > fins;
		for (it = selectedFins.begin(); it != selectedFins.end(); it++) {
			/* Get our fin based upon *it, which is the ID. */
			fins.push_back(dlg->mDatabase->getItemAbsolute(*it));
		}

		SaveFileChooserDialog *fsChooserDlg = new SaveFileChooserDialog(dlg->mDatabase,
												NULL,
												NULL,
												NULL,
												dlg->mOptions,
												dlg->mDialog,
												SaveFileChooserDialog::saveMultipleFinz,
												&fins);
		saved=fsChooserDlg->run_and_respond();

		vector<DatabaseFin<ColorImage>* >::iterator finsit;
		for (finsit = fins.begin(); finsit != fins.end(); finsit++) {
			delete ((DatabaseFin<ColorImage>*) *finsit);
		}
		
	}

	if (saved)
		delete dlg;

}

//*******************************************************************
//***2.02 - new callback to save full-size, full-resolution DARWIN modified images
void on_finzDialogButtonSaveImages_clicked(
		GtkButton *button,
		gpointer userData)
{
	ExportFinzDialog *dlg = (ExportFinzDialog *) userData;

	if (NULL == dlg)
		return;

	bool saved = false;

	/* dlg->num_selected_rows is a "set" of individuals IDs. */
	set<long> selectedFins = dlg->num_selected_rows;

	if (selectedFins.empty()) {//idiot, selected something first for export
		return;
	} else {// one or more fins selected
		set<long>::iterator it;
		vector<DatabaseFin<ColorImage>* > fins;
		for (it = selectedFins.begin(); it != selectedFins.end(); it++) {
			fins.push_back(dlg->mDatabase->getItemAbsolute(*it));
		}

		SaveFileChooserDialog *fsChooserDlg = new SaveFileChooserDialog(dlg->mDatabase,
												NULL,
												NULL,
												NULL,
												dlg->mOptions,
												dlg->mDialog,
												SaveFileChooserDialog::saveFullSizeModImages,
												&fins);
		saved=fsChooserDlg->run_and_respond();

		vector<DatabaseFin<ColorImage>* >::iterator finsit;
		for (finsit = fins.begin(); finsit != fins.end(); finsit++) {
			delete ((DatabaseFin<ColorImage>*) *finsit);
		}
		
	}

	if (saved)
		delete dlg;


}

//*******************************************************************
//
void on_finzDialogButtonSaveAsCatalog_clicked(
		GtkButton *button,
		gpointer userData)
{
	ExportFinzDialog *dlg = (ExportFinzDialog *) userData;

	if (NULL == dlg)
		return;

	// do somthing here to make catalog creation happen

}

//*******************************************************************
//
void on_finzDialogButtonCancel_clicked(
		GtkButton *button,
		gpointer userData)
{
	ExportFinzDialog *dlg = (ExportFinzDialog *) userData;

	if (NULL == dlg)
		return;

	delete dlg;
}
