//*******************************************************************
//   file: Database.cxx
//
// author: J H Stewman (7/8/2008)
//
// This ABSTRACT class is the parent for ALL present and future
// types of database implementations.  Current derived classes
// include ...
//    OldDatabase -- the flat file version
//    SQLiteDatabase -- the first SQL relational database version
//
//*******************************************************************

#include "Database.h"

using namespace std;

//*******************************************************************
//
// Returns number of records in the Individuals table.
//

std::list<DBIndividual> Database::DatabaseRows()
{
	return mIndividualsRecords;
}

//*******************************************************************
//
// Returns number of records in the Individuals table.
//

std::vector<std::list<DBIndividual>> Database::DatabaseRowsByDamage()
{
	return mIndividualsbyDamage;
}

//*******************************************************************
//
// Returns number of fins in lists.
//

unsigned Database::size() const
{
	/* Soon to be removed. KLY */
	return mNameList.size();
}

//*******************************************************************
//
// Returns number of fins in AbsoluteOffset list.
//

unsigned Database::sizeAbsolute() const
{
	/* Soon to be removed. KLY */
	return mAbsoluteOffset.size();
}

//*******************************************************************
//

bool Database::isEmpty() const
{
	return (mNameList.size() == 0);
}

//*******************************************************************
//
string Database::catCategoryName(int id)
{
	string name = "";
	if ((0 <= id) && (id < mCatCategoryNames.size()))
		name = mCatCategoryNames[id];
	return name;
}

//*******************************************************************
//
string Database::catSchemeName()
{
	return mCatSchemeName;
}

//*******************************************************************
//
int Database::catCategoryNamesMax()
{
	return mCatCategoryNames.size();
}

//*******************************************************************
//
void Database::appendCategoryName(string name)
{
	mCatCategoryNames.push_back(name);
}

//*******************************************************************
//
void Database::setCatSchemeName(string name)
{
	mCatSchemeName = name;
}

//*******************************************************************
//
void Database::clearCatalogScheme()
{
	mCatCategoryNames.clear();
}


//*******************************************************************
//
CatalogScheme Database::catalogScheme()
{
	CatalogScheme cat;
	cat.schemeName = mCatSchemeName;
	cat.categoryNames = mCatCategoryNames;

	return cat;
}

//*******************************************************************
//
Database::db_status_t Database::status() const
{
	return mDBStatus;
}

//*******************************************************************
//***1.85
//

db_sort_t Database::currentSort()
{
	return mCurrentSort;
}

//*******************************************************************
//

void Database::sort(db_sort_t sortBy)
{
	mCurrentSort = sortBy;
}

//*******************************************************************
//***1.85 - returns position of id in mIDList as currently sorted
//

int Database::getIDListPosit(std::string id)
{
	// NOTE: this list in the database contains strings, but the strings
	// are made up of two parts (The Dolphin ID -- a string -- and
	// an integer that either the data offset in the old database
	// or the unique numerical id of the fin 
	// in the Invidiuals table in the SQLite database.)
	
	std::vector<std::string>::iterator it;

	bool found(false);
	int posit(0);
	it = mIDList.begin();

	while ((!found) && (it != mIDList.end()))
	{
		std::string listID = *it;
		listID = listID.substr(0,listID.rfind(" ")); // strip the offset
		// should we ignore CASE????
		if (id == listID)
			found = true;
		else
		{
			++it;
			posit++;
		}
	}
	if (! found)
		posit = NOT_IN_LIST;

	return posit;
}



// *****************************************************************************
//
// Returns db filename
//

string Database::getFilename() {

	return mFilename;
}


// *****************************************************************************
//
// Constructors - plain vanilla (***1.99 - mod by JHS)
//

Database::Database(Options *o, CatalogScheme cat, bool createEmptyDB) 
	:	dbOpen(false),
		mFilename(o->mDatabaseFileName),
		mCurrentSort(DB_SORT_NAME),
		mDBStatus(errorLoading),
		mCatSchemeName("")
{
	mCatCategoryNames.clear();
	// set the category names for the database here initially for a create situation
	if (createEmptyDB)
	{
		mCatSchemeName = cat.schemeName;
		mCatCategoryNames = cat.categoryNames;
	}
}

// called ONLY by DummyDatabase constructor

Database::Database() 
	:	dbOpen(false),
		mFilename("DummyDatabase"),
		mCurrentSort(DB_SORT_NAME),
		mDBStatus(errorLoading),
		mCatSchemeName("")
{
	mCatCategoryNames.clear();
}
